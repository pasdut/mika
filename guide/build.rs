use std::io::Write;

fn main() {
    println!("cargo:rerun-if-changed=content");

    let out_dir = std::env::var("OUT_DIR").unwrap();
    let dest_path = std::path::Path::new(&out_dir).join("mika_guide_content_from_md_files.rs");

    let mut sections = collect_sections();
    sections.iter().for_each(|section| {
        if section.main_file.is_none() {
            panic!(
                "The main `.md` file for section '{}' is missing. ",
                section.index
            );
        }
    });
    sections.sort_by(|a, b| a.index.cmp(&b.index));
    sections
        .iter_mut()
        .for_each(|section| section.tabs.sort_by(|a, b| a.index.cmp(&b.index)));

    let sections: Vec<CompiledSection> = sections
        .into_iter()
        .map(|section| section.compile(&out_dir))
        .collect();

    generate_const_array_of_guide_content(&dest_path, &sections);
}

// Section or Chapter? (not sure, but I am sure it's not very important in this context :D)
// A section main contains a single file (00-Introduction.md) or multiple files
// (02-Demo_First-example.md, 02.1-Introduction.md)
struct Section {
    index: usize,
    // A section always has `title` and `main_file`, but the main file does not always come first
    // when walking the `./content` directory.
    title: Option<String>,
    main_file: Option<std::path::PathBuf>,
    tabs: Vec<SectionTab>,
}

struct SectionTab {
    index: usize,
    title: String,
    content_file: std::path::PathBuf,
}

// `rustdoc 02.1-Introduction.md` out put a file named `02.html`
// `rustdoc 02.1-Component.md` also out put a file named `02.html`
// It is obviously a bug in rustdoc, may be it use a `file_stem` and set extension to `html`
// TODO: open an issue on rustdoc

// So, as a workaround, we need to read the file immediately after compiling to html

struct CompiledSection {
    title: String,
    content: String,
    tabs: Vec<CompiledSectionTab>,
}

struct CompiledSectionTab {
    title: String,
    content: String,
}

impl Section {
    fn compile(self, out_dir: &str) -> CompiledSection {
        let title = self.title.unwrap();
        let main_file = self.main_file.unwrap();
        if self.tabs.is_empty() {
            let content = compile_and_get_content(out_dir, &main_file);
            CompiledSection {
                title,
                content,
                tabs: Vec::new(),
            }
        } else {
            let tabs = self
                .tabs
                .into_iter()
                .map(|tab| tab.compile(out_dir))
                .collect();
            CompiledSection {
                title,
                content: std::fs::read_to_string(&main_file)
                    .expect(&format!("Reading file `{}`", main_file.to_string_lossy())),
                tabs,
            }
        }
    }
}

impl SectionTab {
    fn compile(self, out_dir: &str) -> CompiledSectionTab {
        CompiledSectionTab {
            title: self.title,
            content: compile_and_get_content(out_dir, &self.content_file),
        }
    }
}

// Collect sections from `guide/content`
// The main file name's format: `XX-The-title-of-the-section.md` (where XX is the index of the section)
//  * Section.index: parse the `XX` from the file name.
//  * Section.title: the portion after the index (exclude the extension), replaced `-` by space.
// Section with demo contains one or more tabs
//  * The main file (XX-Section-title) contains the demo name (a variant of enum Demo in `guide/src/lib/rs`)
//  * The content of each tab is placed in a separated file named: `XX.Y-Title-of-the-tab`
//  (where XX, is the index of the section, Y is the order (index) of the tab)
fn collect_sections() -> Vec<Section> {
    let mut sections = Vec::new();
    for entry in std::fs::read_dir("./content").expect("./content") {
        if let Ok(entry) = entry {
            let p = entry.path();
            if let Some(ext) = p.extension() {
                if ext == "md" {
                    if let Some(file_name) = p.file_stem() {
                        let file_name = file_name.to_string_lossy();
                        let parts: Vec<&str> = file_name.split("-").collect();
                        if let Ok((main_index, sub_index)) = parse_indices(parts[0]) {
                            add_content(
                                &mut sections,
                                main_index,
                                sub_index,
                                parts[1..].join(" "),
                                p.to_path_buf(),
                            );
                        }
                    }
                }
            }
        }
    }
    sections
}

// Parse the index parts of the file name:
//  * 00 -> (0, None)
//  * 02.1 -> (2, Some(1))
fn parse_indices(values: &str) -> Result<(usize, Option<usize>), String> {
    if let Ok(index) = values.parse::<usize>() {
        Ok((index, None))
    } else {
        let values: Vec<&str> = values.split(".").collect();
        let index = values[0].parse::<usize>().map_err(|e| e.to_string())?;
        let sub_index = values[1].parse::<usize>().map_err(|e| e.to_string())?;
        Ok((index, Some(sub_index)))
    }
}

fn add_content(
    sections: &mut Vec<Section>,
    main_index: usize,
    sub_index: Option<usize>,
    title: String,
    path: std::path::PathBuf,
) {
    if let Some(ref mut section) = sections.iter_mut().find(|s| s.index == main_index) {
        // There already exits a section with index = main_index
        if let Some(sub_index) = sub_index {
            // The file being added is a tab in the section
            section.tabs.push(SectionTab {
                index: sub_index,
                title,
                content_file: path,
                //source_content_file: path,
            });
        } else {
            // The file being added is a main file
            if let Some(ref p) = section.main_file {
                panic!(
                    "Two files with the same index: `{}` and `{}`",
                    p.to_string_lossy(),
                    path.to_string_lossy()
                );
            }
            section.title = Some(title);
            section.main_file = Some(path);
        }
    } else {
        // It is a new section
        if let Some(sub_index) = sub_index {
            // But we encounter the tab file before the main file
            sections.push(Section {
                index: main_index,
                title: None,
                main_file: None,
                tabs: vec![SectionTab {
                    index: sub_index,
                    title,
                    content_file: path,
                    //source_content_file: path,
                }],
            })
        } else {
            // We are in luck, the main file come first
            // or, simply it's the only file of the section
            sections.push(Section {
                index: main_index,
                title: Some(title),
                main_file: Some(path),
                tabs: Vec::new(),
            });
        }
    }
}

fn compile_and_get_content(out_dir: &str, path: &std::path::PathBuf) -> String {
    match std::process::Command::new("rustdoc")
        .arg("--output")
        .arg(out_dir)
        .arg("--markdown-no-toc")
        .arg(&path)
        .status()
    {
        Ok(status) => {
            if !status.success() {
                panic!("rustdoc report an error on `{}`", path.to_string_lossy());
            }
            let mut out_put = std::path::PathBuf::from(out_dir);
            out_put.push(path.file_name().expect("a file name without extension"));
            out_put.set_extension("html");
            return read_the_content_of_html_body(&out_put);
        }
        Err(e) => panic!(
            "error on trying to run rustdoc on `{}`: {}",
            path.to_string_lossy(),
            e
        ),
    }
}

fn generate_const_array_of_guide_content(
    dest_path: &std::path::PathBuf,
    sections: &Vec<CompiledSection>,
) {
    let mut f = std::fs::File::create(dest_path).unwrap();
    writeln!(
        f,
        "lazy_static::lazy_static! {{ static ref GUIDE_SECTIONS: [GuideSection;{}] = [",
        sections.len()
    )
    .expect("Writing `const GUIDE_SECTIONS`");
    for section in sections {
        if section.tabs.is_empty() {
            writeln!(
                f,
                "\tGuideSection {{ title: \"{}\", content: SectionContent::CompiledHtml(r#####\"{}\"#####) }}, ",
                section.title,
                section.content
            )
            .expect("Writing `GuideSection {}`");
        } else {
            writeln!(f,"\tGuideSection {{ title: \"{}\", content: SectionContent::ExampleWithDemo{{demo: {}, tabs: vec![",
                section.title,
                section.content
            )
            .expect("Writing `GuideSection {}`");

            for tab in section.tabs.iter() {
                writeln!(
                    f,
                    "TabContent{{ title: \"{}\", compiled_html: r#####\"{}\"##### }},",
                    tab.title, tab.content
                )
                .expect("Writing `TabContent`");
            }
            writeln!(f, "]}} }}, ").expect("Closing the guide sections array");
        }
    }
    writeln!(f, "];}}").expect("Writing closing `]`");
}

// This will read the html file specified by `path`, return turn a portion starting with
// the first`<h1` to the closing `</body>` (but not included it)
fn read_the_content_of_html_body(path: &std::path::PathBuf) -> String {
    let all =
        std::fs::read_to_string(path).expect(&format!("Reading file `{}`", path.to_string_lossy()));
    let start = all.find("<h1").expect(&format!(
        "not found tag `<h1>` in {}",
        path.to_string_lossy()
    ));
    let end = all.rfind("</body>").expect(&format!(
        "not found tag `</body>` in {}",
        path.to_string_lossy()
    ));
    all.split_at(end).0.split_at(start).1.to_string()
}
