# Build and run a Mika app

* You must [install Rust](https://www.rust-lang.org/en-US/)
* `rustup target add wasm32-unknown-unknown` (because we build for the web browser)

## With wasm-pack
* `cargo install wasm-pack`
* `git clone https://gitlab.com/limira-rs/mika.git`
* `cd examples/counter`
* `wasm-pack build`
* Make sure you have the `index.html` in `./pkg` (see in section *First example*)
  * Or you may want to use `simi-cli` (see below, it will generate the default `index.html` for you)
* In `./pkg`, run your favorite static file serve, for example:
  * python3 -m http.server (available on some linux systems)
  * or use `simi-cli` (see below)
* In your browser, visit the address where your static server serves your app.

## With simi-cli
*(Curious? `simi-cli` was built for Simi - an incremental front-end framework in Rust. When starting Mika I don't think it will replace Simi, but with current features, Mika covers goals I try to achieve with Simi)*

`simi-cli` automatically provide a default `index.html` for you, and it also have a simple static file server.

* `cargo install wasm-bindgen-cli` (`Mika` build on top of `wasm-bindgen`)
* `cargo install simi-cli`
* `git clone https://gitlab.com/limira-rs/mika.git`
* `cd examples/counter`
* `simi serve` (yes, it's `simi serve`, because we use `simi-cli`)

Wait for the build to complete, then open your browser and visit http://localhost:8000/