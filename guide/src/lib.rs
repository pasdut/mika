use mika::prelude::*;
use signals::signal::{Mutable, SignalExt};
use wasm_bindgen::prelude::*;

struct GuideSection {
    title: &'static str,
    content: SectionContent,
}

enum SectionContent {
    CompiledHtml(&'static str),
    ExampleWithDemo { demo: Demo, tabs: Vec<TabContent> },
}

pub struct TabContent {
    pub title: &'static str,
    pub compiled_html: &'static str,
}

enum Demo {
    None,
    Counter,
    StatelessRender,
    StatefulComponents,
    Fetch,
}

include!(concat!(
    env!("OUT_DIR"),
    "/mika_guide_content_from_md_files.rs"
));

impl GuideSection {
    fn render(&'static self, comp: &mika::CompHandle<MikaGuideRender>) -> mika::dom::Div {
        let active_section = mika::dom::Div::new().id("active-section");
        let active_section_content = mika::dom::Div::new().id("active-section-content");
        match self.content {
            SectionContent::CompiledHtml(content) => {
                let section_content = mika::dom::Div::new().class("bottom-space right-space");
                section_content.ws_element().set_inner_html(content);

                active_section
                    .child(
                        active_section_content
                            .class("full-width")
                            .child(section_content),
                    )
                    .into()
            }
            SectionContent::ExampleWithDemo { ref demo, ref tabs } => active_section
                .child(
                    active_section_content.class("space-for-demo").child_signal(
                        comp.render()
                            .current_tab_index
                            .signal()
                            .map(move |current_tab_index| {
                                let section_content = mika::dom::Div::new().class("bottom-space");
                                section_content
                                    .ws_element()
                                    .set_inner_html(tabs[current_tab_index].compiled_html);
                                section_content
                            }),
                    ),
                )
                .child(
                    mika::dom::Div::new()
                        .id("active-section-demo")
                        .child(demo.render()),
                )
                .into(),
        }
    }
}

impl Demo {
    fn render(&self) -> mika::dom::FieldSet {
        let demo_container = mika::dom::FieldSet::new()
            .child(mika::dom::Legend::new().text("Demo for the selected section"));

        match self {
            Demo::None => demo_container.text("No demo").into(),
            Demo::Counter => demo_container
                .component(share_lib::counter::CounterComp::default_state())
                .into(),
            Demo::StatelessRender => demo_container
                .component(share_lib::counters::Counters::default_state())
                .into(),
            Demo::StatefulComponents => demo_container
                .component(share_lib::stateful_components::MainComp::default_state())
                .into(),
            Demo::Fetch => demo_container
                .component(share_lib::fetch::FetchComp::default_state())
                .into(),
        }
    }
}

#[derive(Default)]
struct MikaGuide {
    current_section_index: usize,
    current_tab_index: usize,
}

impl MikaGuide {
    fn set_section_index(&mut self, index: usize) {
        self.current_section_index = index;
        self.current_tab_index = 0;
    }

    fn set_tab_index(&mut self, index: usize) {
        self.current_tab_index = index;
    }
}

#[derive(Default)]
struct MikaGuideRender {
    current_section_index: Mutable<usize>,
    current_tab_index: Mutable<usize>,
}

mika::create_app_handle!(MikaGuideRender);

impl mika::Component for MikaGuideRender {
    type State = MikaGuide;
    type ListChange = ();
    type Output = mika::dom::Body;

    fn diff(&self, state: &MikaGuide, _: ()) {
        self.current_section_index
            .set_neq(state.current_section_index);
        self.current_tab_index.set_neq(state.current_tab_index);
    }

    fn render(&self, comp: &mika::CompHandle<Self>) -> Self::Output {
        mika::dom::Body::get()
            .clear()
            .child(mika::dom::Div::new().id("top").class("padding")
                .child(mika::dom::B::new().text("Mika"))
                .child(mika::dom::I::new()
                .text(" is framework for building front-end app in Rust, it tries to help, but may cause annoyances"))
                .child(mika::dom::A::new().class("right").text("GitLab repo").href("https://gitlab.com/limira-rs/mika"))
            )
            .child(mika::dom::Div::new().id("main")
            .child(table_of_content(comp))
            .child_signal(self.current_section_index.signal().map({
                let comp = comp.clone();
                move |current_section_index| {
                GUIDE_SECTIONS[current_section_index].render(&comp)
            }})))
    }
}

fn table_of_content(comp: &mika::CompHandle<MikaGuideRender>) -> mika::dom::Nav {
    mika::dom::Nav::new()
        .id("toc")
        .child(
            mika::dom::Div::new()
                .id("guide-title")
                .class("padding")
                .text("Mika guide"),
        )
        .child(
            mika::dom::Div::new().child_list(GUIDE_SECTIONS.iter().enumerate().map(
                |(index, gs)| {
                    let item = mika::dom::Div::new().child(
                        mika::dom::Div::new()
                            .class("toc-item")
                            .class_if_signal(
                                "selected",
                                comp.render().current_section_index.signal().map(
                                    move |current_section_index| current_section_index == index,
                                ),
                            )
                            .on_click(mika::handler!(comp, MikaGuide::set_section_index(index)))
                            .text(gs.title),
                    );

                    match gs.content {
                        SectionContent::ExampleWithDemo { ref tabs, .. } if tabs.len() > 1 => item
                            .child(
                                mika::dom::Div::new()
                                    .class_if_signal(
                                        "hidden",
                                        comp.render().current_section_index.signal().map(
                                            move |current_section_index| {
                                                current_section_index != index
                                            },
                                        ),
                                    )
                                    .child_list(tabs.iter().enumerate().map({
                                        let active_index = comp.render().current_tab_index.clone();
                                        move |(index, tab)| {
                                            mika::dom::Div::new()
                                                .class("toc-sub-item")
                                                .class_if_signal(
                                                    "selected",
                                                    active_index.signal().map(
                                                        move |active_index| index == active_index,
                                                    ),
                                                )
                                                .on_click(mika::handler!(
                                                    comp,
                                                    MikaGuide::set_tab_index(index)
                                                ))
                                                .text(tab.title)
                                        }
                                    })),
                            ),
                        _ => item,
                    }
                },
            )),
        )
        .into()
}
