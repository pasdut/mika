# `Mika` [WIP]
*A signal-based framework for building front-end app, it tries to help, but may cause annoyances*.

**This is still in experimental stage. Breaking changes occur frequently.**

**You can find [the Guide + demos here](https://limira-rs.gitlab.io/mika/)**
