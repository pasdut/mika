// This file is not a mod, it is include! into the src/dom/mod.rs
create_builders_for_html_elements! {

    // InteractiveContent => (if the href attribute is present)
    // Content model => (Transparent, but there must be no interactive content or a element descendants.)
    /// TODO: Documentation for `<a>`.
    /// [Specification](https://html.spec.whatwg.org/#the-a-element).
    a A ChildedA: AllowText FlowContent InteractiveContent PalpableContent PhrasingContent [
        FlowNonInteractiveContent
    ]

    /// TODO: Documentation for `<abbr>`.
    /// [Specification](https://html.spec.whatwg.org/#the-abbr-element).
    abbr Abbr ChildedAbbr: AllowText FlowContent PalpableContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        PhrasingContent
    ]

    // Content model => (but with no heading content descendants, no sectioning content descendants, and no header, footer, or address element descendants.)
    /// TODO: Documentation for `<address>`.
    /// [Specification](https://html.spec.whatwg.org/#the-address-element).
    address Address ChildedAddress: AllowText FlowContent PalpableContent FlowNonInteractiveContent [
        FlowContent
    ]

    // FlowContent => (if it is a descendant of a map element)
    // PhrasingContent => (if it is a descendant of a map element)
    /// TODO: Documentation for `<area>`.
    /// [Specification](https://html.spec.whatwg.org/#the-area-element).
    area Area: FlowContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent []

    /// TODO: Documentation for `<article>`.
    /// [Specification](https://html.spec.whatwg.org/#the-article-element).
    article Article ChildedArticle: AllowText FlowContent PalpableContent SectioningContent FlowNonInteractiveContent [
        FlowContent
    ]

    /// TODO: Documentation for `<aside>`.
    /// [Specification](https://html.spec.whatwg.org/#the-aside-element).
    aside Aside ChildedAside: AllowText FlowContent PalpableContent SectioningContent FlowNonInteractiveContent [
        FlowContent
    ]

    // InteractiveContent => (if the controls attribute is present)
    // PalpableContent => (if the controls attribute is present)
    // Content model => (If the element has a src attribute: zero or more track elements, then transparent, but with no media element descendants. If the element does not have a src attribute: zero or more source elements, then zero or more track elements, then transparent, but with no media element descendants.)
    /// TODO: Documentation for `<audio>`.
    /// [Specification](https://html.spec.whatwg.org/#the-audio-element).
    audio Audio ChildedAudio: AllowText EmbeddedContent FlowContent InteractiveContent PalpableContent PhrasingContent [
        ChildOfAudio: FlowContent + [
            Track
            Source
        ]
    ]

    /// TODO: Documentation for `<b>`.
    /// [Specification](https://html.spec.whatwg.org/#the-b-element).
    b B ChildedB: AllowText FlowContent PalpableContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        PhrasingContent
    ]

    /// TODO: Documentation for `<bdi>`.
    /// [Specification](https://html.spec.whatwg.org/#the-bdi-element).
    bdi Bdi ChildedBdi: AllowText FlowContent PalpableContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        PhrasingContent
    ]

    /// TODO: Documentation for `<bdo>`.
    /// [Specification](https://html.spec.whatwg.org/#the-bdo-element).
    bdo Bdo ChildedBdo: AllowText FlowContent PalpableContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        PhrasingContent
    ]

    /// TODO: Documentation for `<blockquote>`.
    /// [Specification](https://html.spec.whatwg.org/#the-blockquote-element).
    blockquote BlockQuote ChildedBlockQuote: AllowText FlowContent PalpableContent FlowNonInteractiveContent [
        FlowContent
    ]

    /// TODO: Documentation for `<br>`.
    /// [Specification](https://html.spec.whatwg.org/#the-br-element).
    br Br: FlowContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent []

    // Content model => (but there must be no interactive content descendant.)
    /// TODO: Documentation for `<button>`.
    /// [Specification](https://html.spec.whatwg.org/#the-button-element).
    button Button ChildedButton: AllowText FlowContent InteractiveContent PalpableContent PhrasingContent [
        PhrasingNonInteractiveContent
    ]

    // Content model => (Transparent, https://html.spec.whatwg.org/#canvas)
    /// TODO: Documentation for `<canvas>`.
    /// [Specification](https://html.spec.whatwg.org/#canvas).
    canvas Canvas ChildedCanvas: AllowText EmbeddedContent FlowContent PalpableContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        FlowContent
    ]

    // Content model => (but with no descendant table elements.)
    /// TODO: Documentation for `<caption>`.
    /// [Specification](https://html.spec.whatwg.org/#the-caption-element).
    caption Caption ChildedCaption: AllowText [
        FlowContent
    ]

    /// TODO: Documentation for `<cite>`.
    /// [Specification](https://html.spec.whatwg.org/#the-cite-element).
    cite Cite ChildedCite: AllowText FlowContent PalpableContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        PhrasingContent
    ]

    /// TODO: Documentation for `<code>`.
    /// [Specification](https://html.spec.whatwg.org/#the-code-element).
    code Code ChildedCode: AllowText FlowContent PalpableContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        PhrasingContent
    ]

    /// TODO: Documentation for `<col>`.
    /// [Specification](https://html.spec.whatwg.org/#the-col-element).
    col Col []

    // Content model => (If the span attribute is present: Nothing.)
    /// TODO: Documentation for `<colgroup>`.
    /// [Specification](https://html.spec.whatwg.org/#the-colgroup-element).
    colgroup ColGroup ChildedColGroup [
        ChildOfColGroup [
            Col
            Template
        ]
    ]

    /// TODO: Documentation for `<data>`.
    /// [Specification](https://html.spec.whatwg.org/#the-data-element).
    data Data ChildedData: AllowText FlowContent PalpableContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        PhrasingContent
    ]

    // Content model => (Either: phrasing content. Or: Zero or more option and script-supporting elements.)
    /// TODO: Documentation for `<datalist>`.
    /// [Specification](https://html.spec.whatwg.org/#the-datalist-element).
    datalist DataList ChildedDataList: AllowText FlowContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        ChildOfDataList: PhrasingContent + [
            Option
        ]
    ]

    /// TODO: Documentation for `<dd>`.
    /// [Specification](https://html.spec.whatwg.org/#the-dd-element).
    dd Dd ChildedDd: AllowText [
        FlowContent
    ]

    // Content model => (Transparent)
    /// TODO: Documentation for `<del>`.
    /// [Specification](https://html.spec.whatwg.org/#the-del-element).
    del Del ChildedDel: AllowText FlowContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        FlowContent
    ]

    // Content model => (One summary element followed by flow content.)
    /// TODO: Documentation for `<details>`.
    /// [Specification](https://html.spec.whatwg.org/#the-details-element).
    details Details ChildedDetails: AllowText FlowContent InteractiveContent PalpableContent [
        ChildOfDetails: FlowContent + [
            Summary
            ChildedSummary
        ]
    ]

    // Content model => (but there must be no dfn element descendants.)
    /// TODO: Documentation for `<dfn>`.
    /// [Specification](https://html.spec.whatwg.org/#the-dfn-element).
    dfn Dfn ChildedDfn: AllowText FlowContent PalpableContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        PhrasingContent
    ]

    /// TODO: Documentation for `<dialog>`.
    /// [Specification](https://html.spec.whatwg.org/#the-dialog-element).
    dialog Dialog ChildedDialog: AllowText FlowContent FlowNonInteractiveContent [
        FlowContent
    ]

    // Content model => (If the element is a child of a dl element: one or more dt elements followed by one or more dd elements, optionally intermixed with script-supporting elements.)
    /// TODO: Documentation for `<div>`.
    /// [Specification](https://html.spec.whatwg.org/#the-div-element).
    div Div ChildedDiv: AllowText FlowContent PalpableContent FlowNonInteractiveContent [
        ChildOfDiv: FlowContent + [
            Dd
            ChildedDd
            Dt
            ChildedDt
        ]
    ]

    // PalpableContent => (if the element's children include at least one name-value group)
    // Content model => (Either: Zero or more groups each consisting of one or more dt elements followed by one or more dd elements, optionally intermixed with script-supporting elements. Or: One or more div elements, optionally intermixed with script-supporting elements.)
    /// TODO: Documentation for `<dl>`.
    /// [Specification](https://html.spec.whatwg.org/#the-dl-element).
    dl Dl ChildedDl: FlowContent PalpableContent FlowNonInteractiveContent [
        ChildOfDl [
            Dd
            ChildedDd
            Dt
            ChildedDt
            Div
            ChildedDiv
        ]
    ]

    // Content model => (but with no header, footer, sectioning content, or heading content descendants.)
    /// TODO: Documentation for `<dt>`.
    /// [Specification](https://html.spec.whatwg.org/#the-dt-element).
    dt Dt ChildedDt: AllowText [
        FlowContent
    ]

    /// TODO: Documentation for `<em>`.
    /// [Specification](https://html.spec.whatwg.org/#the-em-element).
    em Em ChildedEm: AllowText FlowContent PalpableContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        PhrasingContent
    ]

    /// TODO: Documentation for `<embed>`.
    /// [Specification](https://html.spec.whatwg.org/#the-embed-element).
    embed Embed: EmbeddedContent FlowContent InteractiveContent PalpableContent PhrasingContent []

    // Content model => (Optionally a legend element, followed by flow content.)
    /// TODO: Documentation for `<fieldset>`.
    /// [Specification](https://html.spec.whatwg.org/#the-fieldset-element).
    fieldset FieldSet ChildedFieldSet: AllowText FlowContent PalpableContent FlowNonInteractiveContent [
        ChildOfFieldSet: FlowContent + [
            Legend
            ChildedLegend
        ]
    ]

    /// TODO: Documentation for `<figcaption>`.
    /// [Specification](https://html.spec.whatwg.org/#the-figcaption-element).
    figcaption FigCaption ChildedFigCaption: AllowText [
        FlowContent
    ]

    // Content model => (figcaption, if include, must be the first or the last?)
    /// TODO: Documentation for `<figure>`.
    /// [Specification](https://html.spec.whatwg.org/#the-figure-element).
    figure Figure ChildedFigure: AllowText FlowContent PalpableContent FlowNonInteractiveContent [
        ChildOfFigure: FlowContent + [
            FigCaption
            ChildedFigCaption
        ]
    ]

    // Content model => (but with no header or footer element descendants.)
    /// TODO: Documentation for `<footer>`.
    /// [Specification](https://html.spec.whatwg.org/#the-footer-element).
    footer Footer ChildedFooter: AllowText FlowContent PalpableContent FlowNonInteractiveContent [
        FlowContent
    ]

    // Content model => (but with no form element descendants.)
    /// TODO: Documentation for `<form>`.
    /// [Specification](https://html.spec.whatwg.org/#the-form-element).
    form Form ChildedForm: AllowText FlowContent PalpableContent FlowNonInteractiveContent [
        FlowContent
    ]

    /// TODO: Documentation for `<h1>`.
    /// [Specification](https://html.spec.whatwg.org/#the-h1-element).
    h1 H1 ChildedH1: AllowText FlowContent HeadingContent PalpableContent FlowNonInteractiveContent [
        PhrasingContent
    ]

    /// TODO: Documentation for `<h2>`.
    /// [Specification](https://html.spec.whatwg.org/#the-h2-element).
    h2 H2 ChildedH2: AllowText FlowContent HeadingContent PalpableContent FlowNonInteractiveContent [
        PhrasingContent
    ]

    /// TODO: Documentation for `<h3>`.
    /// [Specification](https://html.spec.whatwg.org/#the-h3-element).
    h3 H3 ChildedH3: AllowText FlowContent HeadingContent PalpableContent FlowNonInteractiveContent [
        PhrasingContent
    ]

    /// TODO: Documentation for `<h4>`.
    /// [Specification](https://html.spec.whatwg.org/#the-h4-element).
    h4 H4 ChildedH4: AllowText FlowContent HeadingContent PalpableContent FlowNonInteractiveContent [
        PhrasingContent
    ]

    /// TODO: Documentation for `<h5>`.
    /// [Specification](https://html.spec.whatwg.org/#the-h5-element).
    h5 H5 ChildedH5: AllowText FlowContent HeadingContent PalpableContent FlowNonInteractiveContent [
        PhrasingContent
    ]

    /// TODO: Documentation for `<h6>`.
    /// [Specification](https://html.spec.whatwg.org/#the-h6-element).
    h6 H6 ChildedH6: AllowText FlowContent HeadingContent PalpableContent FlowNonInteractiveContent [
        PhrasingContent
    ]

    // Content model => (but with no header or footer element descendants.)
    /// TODO: Documentation for `<header>`.
    /// [Specification](https://html.spec.whatwg.org/#the-header-element).
    header Header ChildedHeader: AllowText FlowContent PalpableContent FlowNonInteractiveContent [
        FlowContent
    ]

    /// TODO: Documentation for `<hgroup>`.
    /// [Specification](https://html.spec.whatwg.org/#the-hgroup-element).
    hgroup Hgroup ChildedHgroup: FlowContent HeadingContent PalpableContent FlowNonInteractiveContent [
        ChildOfHgroup [
            H1
            ChildedH1
            H2
            ChildedH2
            H3
            ChildedH3
            H4
            ChildedH4
            H5
            ChildedH5
            H6
            ChildedH6
        ]
    ]

    /// TODO: Documentation for `<hr>`.
    /// [Specification](https://html.spec.whatwg.org/#the-hr-element).
    hr Hr: FlowContent FlowNonInteractiveContent []

    /// TODO: Documentation for `<i>`.
    /// [Specification](https://html.spec.whatwg.org/#the-i-element).
    i I ChildedI: AllowText FlowContent PalpableContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        PhrasingContent
    ]

    /// TODO: Documentation for `<iframe>`.
    /// [Specification](https://html.spec.whatwg.org/#the-iframe-element).
    iframe Iframe: AllowText EmbeddedContent FlowContent InteractiveContent PalpableContent PhrasingContent []

    // InteractiveContent => (if the usemap attribute is present)
    /// TODO: Documentation for `<img>`.
    /// [Specification](https://html.spec.whatwg.org/#the-img-element).
    img Img: EmbeddedContent FlowContent InteractiveContent PalpableContent PhrasingContent []

    // InteractiveContent => (if the type attribute is not in the Hidden state)
    // PalpableContent => (if the type attribute is not in the Hidden state)
    /// TODO: Documentation for `<input>`.
    /// [Specification](https://html.spec.whatwg.org/#the-input-element).
    input Input: FlowContent InteractiveContent PalpableContent PhrasingContent []

    // Content model => (Transparent)
    /// TODO: Documentation for `<ins>`.
    /// [Specification](https://html.spec.whatwg.org/#the-ins-element).
    ins Ins ChildedIns: AllowText FlowContent PalpableContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        FlowContent
    ]

    /// TODO: Documentation for `<kbd>`.
    /// [Specification](https://html.spec.whatwg.org/#the-kbd-element).
    kbd Kbd ChildedKbd: AllowText FlowContent PalpableContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        PhrasingContent
    ]

    // Content model => (but with no descendant labelable elements unless it is the element's labeled control, and no descendant label elements.)
    /// TODO: Documentation for `<label>`.
    /// [Specification](https://html.spec.whatwg.org/#the-label-element).
    label Label ChildedLabel: AllowText FlowContent InteractiveContent PalpableContent PhrasingContent [
        PhrasingContent
    ]

    /// TODO: Documentation for `<legend>`.
    /// [Specification](https://html.spec.whatwg.org/#the-legend-element).
    legend Legend ChildedLegend: AllowText [
        PhrasingContent
    ]

    /// TODO: Documentation for `<li>`.
    /// [Specification](https://html.spec.whatwg.org/#the-li-element).
    li Li ChildedLi: AllowText [
        FlowContent
    ]

    // FlowContent => (if it is a hierarchically correct mainelement)
    /// TODO: Documentation for `<main>`.
    /// [Specification](https://html.spec.whatwg.org/#the-main-element).
    main Main ChildedMain: AllowText FlowContent PalpableContent FlowNonInteractiveContent [
        FlowContent
    ]

    // Content model => (Transparent)
    /// TODO: Documentation for `<map>`.
    /// [Specification](https://html.spec.whatwg.org/#the-map-element).
    map Map ChildedMap: AllowText FlowContent PalpableContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        FlowContent
    ]

    /// TODO: Documentation for `<mark>`.
    /// [Specification](https://html.spec.whatwg.org/#the-mark-element).
    mark Mark ChildedMark: AllowText FlowContent PalpableContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        PhrasingContent
    ]

    // PalpableContent => (if the element's children include at least one li element)
    /// TODO: Documentation for `<menu>`.
    /// [Specification](https://html.spec.whatwg.org/#the-menu-element).
    menu Menu ChildedMenu: FlowContent PalpableContent FlowNonInteractiveContent [
        ChildOfMenu [
            Li
            ChildedLi
        ]
    ]

    // Content model => (but there must be no meter element descendants.)
    /// TODO: Documentation for `<meter>`.
    /// [Specification](https://html.spec.whatwg.org/#the-meter-element).
    meter Meter ChildedMeter: AllowText FlowContent PalpableContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        PhrasingContent
    ]

    /// TODO: Documentation for `<nav>`.
    /// [Specification](https://html.spec.whatwg.org/#the-nav-element).
    nav Nav ChildedNav: AllowText FlowContent PalpableContent SectioningContent FlowNonInteractiveContent [
        FlowContent
    ]

    // InteractiveContent => (if the usemap attribute is present)
    // Content model => (Transparent)
    /// TODO: Documentation for `<object>`.
    /// [Specification](https://html.spec.whatwg.org/#the-object-element).
    object Object ChildedObject: AllowText EmbeddedContent FlowContent InteractiveContent PalpableContent PhrasingContent [
        ChildOfObject: FlowContent + [
            Param
        ]
    ]

    // PalpableContent => (if the element's children include at least one li element)
    /// TODO: Documentation for `<ol>`.
    /// [Specification](https://html.spec.whatwg.org/#the-ol-element).
    ol Ol ChildedOl: FlowContent PalpableContent FlowNonInteractiveContent [
        ChildOfOl [
            Li
            ChildedLi
        ]
    ]

    /// TODO: Documentation for `<optgroup>`.
    /// [Specification](https://html.spec.whatwg.org/#the-optgroup-element).
    optgroup OptGroup ChildedOptGroup [
        ChildOfOptGroup [
            Option
        ]
    ]

    // Content model => (TODO https://html.spec.whatwg.org/#the-option-element)
    /// TODO: Documentation for `<option>`.
    /// [Specification](https://html.spec.whatwg.org/#the-option-element).
    option Option: AllowText []

    /// TODO: Documentation for `<output>`.
    /// [Specification](https://html.spec.whatwg.org/#the-output-element).
    output Output ChildedOutput: AllowText FlowContent PalpableContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        PhrasingContent
    ]

    /// TODO: Documentation for `<p>`.
    /// [Specification](https://html.spec.whatwg.org/#the-p-element).
    p P ChildedP: AllowText FlowContent PalpableContent FlowNonInteractiveContent [
        PhrasingContent
    ]

    /// TODO: Documentation for `<param>`.
    /// [Specification](https://html.spec.whatwg.org/#the-param-element).
    param Param []

    /// TODO: Documentation for `<picture>`.
    /// [Specification](https://html.spec.whatwg.org/#the-picture-element).
    picture Picture ChildedPicture: EmbeddedContent FlowContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        ChildOfPicture [
            Source
            Img
        ]
    ]

    /// TODO: Documentation for `<pre>`.
    /// [Specification](https://html.spec.whatwg.org/#the-pre-element).
    pre Pre ChildedPre: AllowText FlowContent PalpableContent FlowNonInteractiveContent [
        PhrasingContent
    ]

    // Content model => (but there must be no progress element descendants.)
    /// TODO: Documentation for `<progress>`.
    /// [Specification](https://html.spec.whatwg.org/#the-progress-element).
    progress Progress ChildedProgress: AllowText FlowContent PalpableContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        PhrasingContent
    ]

    /// TODO: Documentation for `<q>`.
    /// [Specification](https://html.spec.whatwg.org/#the-q-element).
    q Q ChildedQ: AllowText FlowContent PalpableContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        PhrasingContent
    ]

    /// TODO: Documentation for `<rp>`.
    /// [Specification](https://html.spec.whatwg.org/#the-rp-element).
    rp Rp: AllowText []

    /// TODO: Documentation for `<rt>`.
    /// [Specification](https://html.spec.whatwg.org/#the-rt-element).
    rt Rt ChildedRt: AllowText [
        PhrasingContent
    ]

    // Content model => (TODO info at https://html.spec.whatwg.org/#the-ruby-element)
    /// TODO: Documentation for `<ruby>`.
    /// [Specification](https://html.spec.whatwg.org/#the-ruby-element).
    ruby Ruby ChildedRuby: AllowText FlowContent PalpableContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        ChildOfRuby: PhrasingContent + [
            Rt
            ChildedRt
            Rp
        ]
    ]

    /// TODO: Documentation for `<s>`.
    /// [Specification](https://html.spec.whatwg.org/#the-s-element).
    s S ChildedS: AllowText FlowContent PalpableContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        PhrasingContent
    ]

    /// TODO: Documentation for `<samp>`.
    /// [Specification](https://html.spec.whatwg.org/#the-samp-element).
    samp Samp ChildedSamp: AllowText FlowContent PalpableContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        PhrasingContent
    ]

    /// TODO: Documentation for `<section>`.
    /// [Specification](https://html.spec.whatwg.org/#the-section-element).
    section Section ChildedSection: AllowText FlowContent PalpableContent SectioningContent FlowNonInteractiveContent [
        FlowContent
    ]

    /// TODO: Documentation for `<select>`.
    /// [Specification](https://html.spec.whatwg.org/#the-select-element).
    select Select ChildedSelect: FlowContent InteractiveContent PalpableContent PhrasingContent [
        ChildOfSelect [
            Option
            OptGroup
            ChildedOptGroup
        ]
    ]

    // Content model => (Transparent)
    /// TODO: Documentation for `<slot>`.
    /// [Specification](https://html.spec.whatwg.org/#the-slot-element).
    slot Slot ChildedSlot: AllowText FlowContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        FlowContent
    ]

    /// TODO: Documentation for `<small>`.
    /// [Specification](https://html.spec.whatwg.org/#the-small-element).
    small Small ChildedSmall: AllowText FlowContent PalpableContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        PhrasingContent
    ]

    /// TODO: Documentation for `<source>`.
    /// [Specification](https://html.spec.whatwg.org/#the-source-element).
    source Source []

    /// TODO: Documentation for `<span>`.
    /// [Specification](https://html.spec.whatwg.org/#the-span-element).
    span Span ChildedSpan: AllowText FlowContent PalpableContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        PhrasingContent
    ]

    /// TODO: Documentation for `<strong>`.
    /// [Specification](https://html.spec.whatwg.org/#the-strong-element).
    strong Strong ChildedStrong: AllowText FlowContent PalpableContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        PhrasingContent
    ]

    /// TODO: Documentation for `<sub>`.
    /// [Specification](https://html.spec.whatwg.org/#the-sub-element).
    sub Sub ChildedSub: AllowText FlowContent PalpableContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        PhrasingContent
    ]

    // Content model => (Either: phrasing content. Or: one element of heading content.)
    /// TODO: Documentation for `<summary>`.
    /// [Specification](https://html.spec.whatwg.org/#the-summary-element).
    summary Summary ChildedSummary: AllowText [
        ChildOfSummary: PhrasingContent + [
            H1
            ChildedH1
            H2
            ChildedH2
            H3
            ChildedH3
            H4
            ChildedH4
            H5
            ChildedH5
            H6
            ChildedH6
            Hgroup
            ChildedHgroup
        ]
    ]

    /// TODO: Documentation for `<sup>`.
    /// [Specification](https://html.spec.whatwg.org/#the-sup-element).
    sup Sup ChildedSup: AllowText FlowContent PalpableContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        PhrasingContent
    ]

    // Content model => (In this order: optionally a caption element, followed by zero or more colgroup elements, followed optionally by a thead element, followed by either zero or more tbody elements or one or more tr elements, followed optionally by a tfoot element, optionally intermixed with one or more script-supporting elements.)
    /// TODO: Documentation for `<table>`.
    /// [Specification](https://html.spec.whatwg.org/#the-table-element).
    table Table ChildedTable: FlowContent PalpableContent FlowNonInteractiveContent [
        //#[deprecated(note="Adding children by this method may cause disordered children. Please use methods: caption/col_group/thead/tbody(_list/_list_signal/_keyed_list_signal)/tr(_list/_list_signal/_keyed_list_signal)/tfoot.")]
        ChildOfTable [
            Caption
            ChildedCaption
            ColGroup
            ChildedColGroup
            Thead
            ChildedThead
            Tbody
            ChildedTbody
            Tr
            ChildedTr
            Tfoot
            ChildedTfoot
        ]
    ]

    /// TODO: Documentation for `<tbody>`.
    /// [Specification](https://html.spec.whatwg.org/#the-tbody-element).
    tbody Tbody ChildedTbody [
        ChildOfTbody [
            Tr
            ChildedTr
        ]
    ]

    /// TODO: Documentation for `<td>`.
    /// [Specification](https://html.spec.whatwg.org/#the-td-element).
    td Td ChildedTd: AllowText [
        FlowContent
    ]

    /// TODO: Documentation for `<template>`.
    /// [Specification](https://html.spec.whatwg.org/#the-template-element).
    template Template: FlowContent MetadataContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent []

    // Content model => (No content, Mika implement <textarea> text content as a value)
    /// TODO: Documentation for `<textarea>`.
    /// [Specification](https://html.spec.whatwg.org/#the-textarea-element).
    textarea TextArea: FlowContent InteractiveContent PalpableContent PhrasingContent []

    /// TODO: Documentation for `<tfoot>`.
    /// [Specification](https://html.spec.whatwg.org/#the-tfoot-element).
    tfoot Tfoot ChildedTfoot [
        ChildOfTfoot [
            Tr
            ChildedTr
        ]
    ]

    // Content model => (but with no header, footer, sectioning content, or heading content descendants.)
    /// TODO: Documentation for `<th>`.
    /// [Specification](https://html.spec.whatwg.org/#the-th-element).
    th Th ChildedTh: AllowText [
        FlowContent
    ]

    /// TODO: Documentation for `<thead>`.
    /// [Specification](https://html.spec.whatwg.org/#the-thead-element).
    thead Thead ChildedThead [
        ChildOfThead [
            Tr
            ChildedTr
        ]
    ]

    // Content model => (If the element has a datetime attribute: Phrasing content. Otherwise: Text, but must match requirements described in prose below.)
    /// TODO: Documentation for `<time>`.
    /// [Specification](https://html.spec.whatwg.org/#the-time-element).
    time Time ChildedTime: AllowText FlowContent PalpableContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        PhrasingContent
    ]

    /// TODO: Documentation for `<tr>`.
    /// [Specification](https://html.spec.whatwg.org/#the-tr-element).
    tr Tr ChildedTr [
        ChildOfTr [
            Td
            ChildedTd
            Th
            ChildedTh
        ]
    ]

    /// TODO: Documentation for `<track>`.
    /// [Specification](https://html.spec.whatwg.org/#the-track-element).
    track Track []

    /// TODO: Documentation for `<u>`.
    /// [Specification](https://html.spec.whatwg.org/#the-u-element).
    u U ChildedU: AllowText FlowContent PalpableContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        PhrasingContent
    ]

    // PalpableContent => (if the element's children include at least one li element)
    /// TODO: Documentation for `<ul>`.
    /// [Specification](https://html.spec.whatwg.org/#the-ul-element).
    ul Ul ChildedUl: FlowContent PalpableContent FlowNonInteractiveContent [
        ChildOfUl [
            Li
            ChildedLi
        ]
    ]

    /// TODO: Documentation for `<var>`.
    /// [Specification](https://html.spec.whatwg.org/#the-var-element).
    var Var ChildedVar: AllowText FlowContent PalpableContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent [
        PhrasingContent
    ]

    // InteractiveContent => (if the controls attribute is present)
    // Content model => (If the element has a src attribute: zero or more track elements, then transparent, but with no media element descendants. If the element does not have a src attribute: zero or more source elements, then zero or more track elements, then transparent, but with no media element descendants.)
    /// TODO: Documentation for `<video>`.
    /// [Specification](https://html.spec.whatwg.org/#the-video-element).
    video Video ChildedVideo: AllowText EmbeddedContent FlowContent InteractiveContent PalpableContent PhrasingContent [
        ChildOfVideo: FlowContent + [
            Track
            Source
        ]
    ]

    /// TODO: Documentation for `<wbr>`.
    /// [Specification](https://html.spec.whatwg.org/#the-wbr-element).
    wbr Wbr: FlowContent PhrasingContent FlowNonInteractiveContent PhrasingNonInteractiveContent []
}
