//! This module implements all standard HTML elements. All elements are
//! built on top of the TotalHtml which implement attributes from
//! all HTML elements. Each specific HTML element is just a wrapper
//! around TotalHtml and just show methods that relevant to it.
//!
//! TODO: Do we need to specialize <input> element? Such as:
//!     mika::dom::Input::file() will create an input with type=file
//!     mika::dom::Input::date() will create an input with type=date
//!     ... and only expose relevant attribute method for that type of input?
//!
//!
//! Mika does not support deprecated or non standard html elements.

use signals::signal_vec::VecDiff;
use wasm_bindgen::{JsCast, UnwrapThrowExt};

#[macro_use]
mod macros;

pub mod traits;
use traits::*;

/// The TotalElement implements all attributes from all HTML elements.
/// It is intended for internal use by Mika and Mika's macros - which
/// the reason for `pub`). HTML elements (`<div>`, `<input>`...) have their
/// equivalent types named in camel case (Div, Input...). Div, Input...
/// are wrapper around TotalElement and only expose relevant methods.
/// We also tries to limit the types of children can be added to each
/// element. For example, `Table::new().child(child_item)`, if the `child_item`
/// is are not be allowed as a child of the element `<table>`, it will
/// fail to compile.
pub struct TotalElement {
    ws_element: web_sys::Element,
    attributes: Vec<Attribute>,
    children: NodeList,
}

include! {"elements.rs"}
include! {"attributes.rs"}

pub enum Attribute {
    Event(Box<dyn crate::events::Listener>),
    Signal(discard::DiscardOnDrop<signals::CancelableFutureHandle>),
}

#[derive(Clone)]
pub struct NodeList(std::sync::Arc<std::sync::Mutex<Vec<Node>>>);

impl NodeList {
    fn new() -> Self {
        Self(std::sync::Arc::new(std::sync::Mutex::new(Vec::new())))
    }

    fn push<T: Into<Node>>(&self, node: T) {
        self.0
            .lock()
            .expect_throw("NodeList::push self.0.lock()")
            .push(node.into());
    }
}

pub enum Node {
    Body(Body),
    Element(TotalElement),
    Text(Text),
    CustomElement(Box<dyn crate::app::FlowContentCustomElement>),
}

impl Node {
    fn insert_at(&self, index: usize, parent: &web_sys::Node) {
        match self {
            Node::Body(_) => {}
            Node::Element(element) => element.insert_at(index, parent),
            Node::Text(_) => {}
            Node::CustomElement(ce) => ce.insert_at(index, parent),
        }
    }

    fn remove_from(&self, parent: &web_sys::Node) {
        match self {
            Node::Body(_) => {}
            Node::Element(element) => element.remove_from(parent),
            Node::Text(_) => {}
            Node::CustomElement(ce) => ce.remove_from(parent),
        }
    }

    pub fn get_text(&mut self) -> std::option::Option<&mut crate::dom::Text> {
        match self {
            Node::Text(ref mut text) => Some(text),
            _ => None,
        }
    }

    pub fn get_element(&mut self) -> std::option::Option<&mut crate::dom::TotalElement> {
        match self {
            Node::Element(ref mut element) => Some(element),
            _ => None,
        }
    }
}

impl From<Body> for Node {
    fn from(item: Body) -> Self {
        Node::Body(item)
    }
}

impl From<TotalElement> for Node {
    fn from(item: TotalElement) -> Self {
        Node::Element(item)
    }
}

impl HasWsNode for TotalElement {
    fn ws_node(&self) -> &web_sys::Node {
        self.ws_element.as_ref()
    }
}

impl StoreHandle for TotalElement {
    fn store_future_handle(
        &mut self,
        handle: discard::DiscardOnDrop<signals::CancelableFutureHandle>,
    ) {
        self.attributes.push(Attribute::Signal(handle));
    }

    fn store_listener(&mut self, listener: Box<dyn crate::events::Listener>) {
        self.attributes.push(Attribute::Event(listener));
    }
}

macro_rules! implement_event_methods {
    ($($method_name:ident $EventType:ident,)+) => {
        $(
            pub fn $method_name<F: crate::events::$EventType>(&mut self, f: F) {
                let e = crate::events::$EventType::on(f, self.ws_node());
                self.store_listener(e);
            }
        )+
    }
}

impl TotalElement {
    implement_event_methods! {
        on_blur Blur,
        on_focus Focus,
        on_click Click,
        on_double_click DoubleClick,
        on_change Change,
        on_key_press KeyPress,
    }
}

implement_attribute_methods! {
    no_signal (accesskey               ) (access_key                  access_key                 ) (&str           ) []
    no_signal (autocapitalize          ) (auto_capitalize             auto_capitalize            ) (:AutoCapitalize) []
    signal    (contenteditable         ) (content_editable            content_editable           ) (bool           ) []
    no_signal (contextmenu             ) (context_menu                context_menu               ) (&str           ) []
    no_signal (dir                     ) (dir                         dir                        ) (:Dir           ) []
    signal    (draggable               ) (draggable                   draggable                  ) (bool           ) []
    signal    (hidden                  ) (hidden                      hidden                     ) (bool           ) []
    no_signal (inputmode               ) (input_mode                  input_mode                 ) (:InputMode     ) []
    no_signal (lang                    ) (lang                        lang                       ) (&str           ) []
    no_signal (spellcheck              ) (spell_check                 spell_check                ) (:SpellCheck    ) []
    signal    (style                   ) (style                       style                      ) (&str           ) []
    // specialized
    // signal    (title                   ) (title                       title                      ) (&str           ) []
}

impl TotalElement {
    fn with_ws_element(ws_element: web_sys::Element) -> Self {
        Self {
            ws_element,
            attributes: Vec::new(),
            children: NodeList::new(),
        }
    }
    fn with_tag(tag: &str) -> Self {
        Self::with_ws_element(
            crate::document()
                .create_element(tag)
                .expect_throw("TotalElement::with_tag"),
        )
    }

    pub fn with_id_and_tag(id: &str, tag: &str) -> Result<Self, crate::errors::Error> {
        match crate::document().get_element_by_id(id) {
            None => Err(crate::errors::Error::ElementByIdNotFound(id.to_string())),
            Some(e) => {
                if e.tag_name().eq_ignore_ascii_case(tag) {
                    Ok(Self::with_ws_element(e))
                } else {
                    Err(crate::errors::Error::WrongTag {
                        expected_tag: tag.to_string(),
                        found_tag: e.tag_name().to_string(),
                    })
                }
            }
        }
    }

    /// For hacking by Mika's macros
    pub fn replace_ws_element(&mut self, ws_node: &web_sys::Node) -> web_sys::Element {
        let ws_element: web_sys::Element = ws_node.clone().unchecked_into();
        std::mem::replace(&mut self.ws_element, ws_element)
    }

    fn spawn_for_each<S, F>(&mut self, signal: S, closure: F)
    where
        S: 'static + signals::signal::Signal,
        F: 'static + FnMut(S::Item),
    {
        self.store_future_handle(crate::spawn::for_each(signal, closure));
    }

    fn spawn_for_each_vec<S, F>(&mut self, signal: S, closure: F)
    where
        S: 'static + signals::signal_vec::SignalVec,
        F: 'static + FnMut(signals::signal_vec::VecDiff<S::Item>),
    {
        self.store_future_handle(crate::spawn::for_each_vec(signal, closure));
    }

    fn attribute_bool(&self, name: &str, value: bool) {
        if value {
            self.ws_element.set_attribute(name, "").expect_throw(
                "TotalElement::attribute_bool: self.ws_element.set_attribute(name, \"\")",
            );
        } else {
            self.ws_element.remove_attribute(name).expect_throw(
                "TotalElement::attribute_bool: self.ws_element.remove_attribute(name)",
            );
        }
    }

    fn attribute_string(&self, name: &str, value: &str) {
        self.ws_element.set_attribute(name, value).expect_throw(
            "TotalElement::attribute_string: self.ws_element.set_attribute(name, value)",
        );
    }

    fn attribute_u32(&self, name: &str, value: u32) {
        self.ws_element
            .set_attribute(name, &value.to_string())
            .expect_throw(
            "TotalElement::attribute_u32: self.ws_element.set_attribute(name, &value.to_string())",
        );
    }

    fn attribute_i32(&self, name: &str, value: i32) {
        self.ws_element
            .set_attribute(name, &value.to_string())
            .expect_throw(
            "TotalElement::attribute_i32: self.ws_element.set_attribute(name, &value.to_string())",
        );
    }

    fn attribute_f64(&self, name: &str, value: f64) {
        self.ws_element
            .set_attribute(name, &value.to_string())
            .expect_throw(
            "TotalElement::attribute_f64: self.ws_element.set_attribute(name, &value.to_string())",
        );
    }

    pub fn custom_attribute(&self, name: &str, value: &str) {
        self.ws_element.set_attribute(name, value).expect_throw(
            "TotalElement::custom_attribute: self.ws_element.set_attribute(name, value)",
        );
    }

    fn id(&self, value: &str) {
        self.ws_element.set_id(value);
    }

    fn title(&self, value: &str) {
        let ws: &web_sys::HtmlElement = self.ws_element.unchecked_ref();
        ws.set_title(value);
    }

    fn add_class(&self, space_separated_classes: &str) {
        let class_list = self.ws_element.class_list();
        space_separated_classes
            .split_ascii_whitespace()
            .for_each(|class| class_list.add_1(class).unwrap_throw());
    }

    fn remove_class(&self, space_separated_classes: &str) {
        let class_list = self.ws_element.class_list();
        space_separated_classes
            .split_ascii_whitespace()
            .for_each(|class| class_list.remove_1(class).unwrap_throw());
    }

    pub fn class_if(&self, space_separated_classes: &str, on: bool) {
        if on {
            self.add_class(space_separated_classes)
        } else {
            self.remove_class(space_separated_classes)
        }
    }

    // TODO: Should this also accept multi-white-space-separated classes?
    pub fn class_if_signal<S>(&mut self, class_name: &'static str, signal: S)
    where
        S: 'static + signals::signal::Signal<Item = bool>,
    {
        let class_list = self.ws_element.class_list();
        let mut current_class_on = false;
        self.spawn_for_each(signal, move |request_class_on| {
            if request_class_on {
                if !current_class_on {
                    current_class_on = true;
                    class_list
                        .add_1(class_name)
                        .expect_throw("TotalElement::class_signal: class_list.add_1(&class_name)");
                }
            } else if current_class_on {
                current_class_on = false;
                class_list
                    .remove_1(class_name)
                    .expect_throw("TotalElement::class_signal: class_list.remove_1(&class_name)");
            }
        });
    }

    pub fn focus_signal<S>(&mut self, signal: S)
    where
        S: 'static + signals::signal::Signal<Item = bool>,
    {
        let ws_element: web_sys::HtmlElement = self
            .ws_element
            .clone()
            .dyn_into()
            .expect_throw("TotalElement::focus_signal: self.ws_element.dyn_into()");
        self.spawn_for_each(signal, move |value| {
            if value {
                if let Err(e) = ws_element.focus() {
                    log::error!("{:?}", e);
                }
            }
        });
    }

    fn text(&self, text: &str) {
        let text = crate::document().create_text_node(text);
        self.ws_node()
            .append_child(&text)
            .expect_throw("TotalElement::text: ws_node.append_child(&text)");
    }

    // ToString is require for working with type like: i32, f64...
    // String? inefficient?
    fn text_signal<S, T>(&mut self, signal: S)
    where
        T: 'static + ToString,
        S: 'static + signals::signal::Signal<Item = T>,
    {
        let text = crate::document().create_text_node("");
        self.ws_node()
            .append_child(&text)
            .expect_throw("TotalElement::text: ws_node.append_child(&text)");
        self.spawn_for_each(signal, move |value| {
            text.set_text_content(Some(&value.to_string()));
        });
    }

    fn child<T: Into<TotalElement>>(&self, child: T) {
        let te = child.into();
        te.append_to(self.ws_node());
        self.children.push(te);
    }

    fn child_signal<S, T>(&mut self, signal: S)
    where
        T: 'static + Into<TotalElement>,
        S: 'static + signals::signal::Signal<Item = T>,
    {
        let parent_node = self.ws_node().clone();
        let children = self.children.clone();

        // This is just a placeholder. Will be replace by the first signal
        let temp_node = TotalElement::with_tag("span");
        temp_node.append_to(&parent_node);

        let mut current_node = temp_node.ws_node().clone();

        let index = {
            let children = &mut children
                .0
                .lock()
                .expect_throw("TotalElement::child_signal: children.0.lock()");
            let index = children.len();
            children.push(temp_node.into());
            index
        };

        self.spawn_for_each(signal, move |element| {
            let children = &mut children
                .0
                .lock()
                .expect_throw("TotalElement::child_signal::spawn_for_each: children.lock()");
            let element = element.into();
            element.replace(&parent_node, &current_node);
            current_node = element.ws_node().clone();
            std::mem::replace(&mut children[index], element.into());
        });
    }

    fn child_list<T, I>(&self, iter: I)
    where
        T: Into<TotalElement>,
        I: std::iter::Iterator<Item = T>,
    {
        let node = self.ws_node();
        let mut children = self
            .children
            .0
            .lock()
            .expect_throw("TotalElement::child_list: self.children.lock()");
        for child in iter {
            let child = child.into();
            child.append_to(node);
            // Do we need to store the child in self.children?
            children.push(child.into());
        }
    }

    // Only use this if you sure that it only runs once. Otherwise
    // it keeps adding new items to self
    fn child_list_signal<E, S>(&mut self, signal: S)
    where
        E: 'static + Into<TotalElement>,
        S: 'static + signals::signal::Signal<Item = Vec<E>>,
    {
        let parent_node = self.ws_node().clone();
        let children = self.children.clone();
        self.spawn_for_each(signal, move |list| {
            let children = &mut children
                .0
                .lock()
                .expect_throw("TotalElement::child_list_signal: children.lock()");
            for child in list {
                let child = child.into();
                child.append_to(&parent_node);
                children.push(child.into());
            }
        });
    }

    fn list_signal_vec<E, S>(&mut self, signal: S)
    where
        E: 'static + Into<TotalElement>,
        S: 'static + signals::signal_vec::SignalVec<Item = E>,
    {
        let parent_node = self.ws_node().clone();
        let children = self.children.clone();
        self.spawn_for_each_vec(signal, move |change| {
            let children = &mut children
                .0
                .lock()
                .expect_throw("TotalElement::list_signal_vec: children.0.lock()");
            match change {
                VecDiff::Replace { values } => {
                    parent_node.set_text_content(None);
                    children.clear();

                    values.into_iter().for_each(|child| {
                        let child = child.into();
                        child.append_to(&parent_node);
                        children.push(child.into());
                    });
                }
                VecDiff::Push { value } => {
                    let value = value.into();
                    value.append_to(&parent_node);
                    children.push(value.into());
                }
                VecDiff::Pop {} => {
                    debug_assert!(!children.is_empty());

                    children
                        .pop()
                        .expect_throw("TotalElement::list_signal_vec: children.pop()")
                        .remove_from(&parent_node);
                }
                VecDiff::InsertAt { index, value } => {
                    let value = value.into();
                    value.insert_at(index, &parent_node);
                    children.insert(index, value.into());
                }
                VecDiff::RemoveAt { index } => {
                    children.remove(index).remove_from(&parent_node);
                }
                VecDiff::UpdateAt { index, value } => {
                    let value = value.into();
                    value.replace_at(index, &parent_node);
                    std::mem::replace(&mut children[index], value.into());
                }
                VecDiff::Move {
                    old_index,
                    new_index,
                } => {
                    let moved = children.remove(old_index);
                    // TODO: Does this need to be removed_from first?
                    // This desperately need a #[test]
                    moved.remove_from(&parent_node);
                    moved.insert_at(new_index, &parent_node);
                    children.insert(new_index, moved);
                }
                VecDiff::Clear {} => {
                    parent_node.set_text_content(None);
                    children.clear();
                }
            }
        });
    }

    fn keyed_list_signal_vec<C, S, T>(&mut self, comp: &crate::app::CompHandle<C>, signal: S)
    where
        C: 'static + crate::app::Component,
        T: 'static + crate::list::KeyedItem<C>,
        <T as crate::list::Key>::Key: Clone,
        S: 'static + signals::signal_vec::SignalVec<Item = T>,
    {
        let comp = comp.clone();
        let parent_node = self.ws_node().clone();
        let keyed_list = std::sync::Arc::new(std::sync::Mutex::new(Vec::new()));
        let template = T::template();
        self.spawn_for_each_vec(signal, move |change| {
            let keyed_list = &mut keyed_list
                .lock()
                .expect_throw("TotalElement::keyed_list_signal_vec: keyed_list.lock()");
            match change {
                VecDiff::Replace { values } => {
                    parent_node.set_text_content(None);
                    keyed_list.clear();

                    values.into_iter().for_each(|value| {
                        let render_output = value.render(&comp, template.clone_deep());
                        render_output.0.append_to(&parent_node);
                        keyed_list.push(KeyedElement::new(&value, render_output));
                    });
                }
                VecDiff::Push { value } => {
                    let render_output = value.render(&comp, template.clone_deep());
                    render_output.0.append_to(&parent_node);
                    keyed_list.push(KeyedElement::new(&value, render_output));
                }
                VecDiff::Pop {} => {
                    debug_assert!(!keyed_list.is_empty());

                    keyed_list
                        .pop()
                        .expect_throw("TotalElement::keyed_list_signal_vec: keyed_list.pop()")
                        .element
                        .remove_from(&parent_node);
                }
                VecDiff::InsertAt { index, value } => {
                    let render_output = value.render(&comp, template.clone_deep());
                    render_output.0.insert_at(index, &parent_node);
                    keyed_list.insert(index, KeyedElement::new(&value, render_output));
                }
                VecDiff::RemoveAt { index } => {
                    keyed_list.remove(index).element.remove_from(&parent_node);
                }
                VecDiff::UpdateAt { index, value } => {
                    let it_is_new = {
                        let keyed_element = keyed_list.get_mut(index).expect_throw(
                            "TotalElement::keyed_list_signal_vec: keyed_list.get(index)",
                        );
                        if value.key().eq(&keyed_element.key) {
                            value.update(
                                &comp,
                                &mut keyed_element.element,
                                &mut keyed_element.to_update,
                            );
                            false
                        } else {
                            true
                        }
                    };
                    if it_is_new {
                        let render_output = value.render(&comp, template.clone_deep());
                        render_output.0.replace_at(index, &parent_node);
                        std::mem::replace(
                            &mut keyed_list[index],
                            KeyedElement::new(&value, render_output),
                        );
                    }
                }
                VecDiff::Move {
                    old_index,
                    new_index,
                } => {
                    let moved = keyed_list.remove(old_index);
                    // TODO: Does this need to be removed_from first?
                    // This desperately need a #[test]
                    moved.element.remove_from(&parent_node);
                    moved.element.insert_at(new_index, &parent_node);
                    keyed_list.insert(new_index, moved);
                }
                VecDiff::Clear {} => {
                    if !keyed_list.is_empty() {
                        parent_node.set_text_content(None);
                        keyed_list.clear();
                    }
                }
            }
        });
    }

    fn raw_component<C, E>(&self, comp: C)
    where
        C: 'static + crate::app::RawComponent<RenderOutput = E>,
        E: FlowContent + Into<TotalElement>,
    {
        use crate::app::FlowContentRawComponent;
        let comp = Box::new(std::rc::Rc::new(comp));
        comp.init();
        let dom = comp.render_in(self.ws_node());
        comp.mounted();
        self.children
            .0
            .lock()
            .expect_throw("TotalElement::raw_component: self.children.0.lock()")
            .push(dom.into());
    }

    fn raw_component_signal<S>(&mut self, signal: S)
    where
        S: 'static + signals::signal::Signal<Item = Box<dyn crate::app::FlowContentRawComponent>>,
    {
        let active_comp = std::sync::Arc::new(std::sync::Mutex::new(None));
        let parent_node = self.ws_node().clone();

        // Similar to child_signal technique
        let temp_node = TotalElement::with_tag("span");
        temp_node.append_to(&parent_node);

        let mut current_node = temp_node.ws_node().clone();

        self.spawn_for_each(signal, move |comp| {
            let mut active_comp = active_comp
                .lock()
                .expect_throw("TotalElement::raw_component_signal: active_comp.lock()");
            if let Some(_old_app) = active_comp.take() {
                // call un_mount on old_app if it is introduced
            }
            comp.init();
            let dom = comp.render_and_replace(&parent_node, &mut current_node);
            comp.mounted();
            active_comp.replace(ActiveComponent {
                _comp: comp,
                _dom: dom,
            });
        });
    }

    fn component<C, E>(&self, mut comp: crate::app::Comp<C>)
    where
        C: 'static + crate::app::Component<Output = E>,
        E: FlowContent + Into<TotalElement>,
    {
        use crate::app::FlowContentComponent;

        // let mut comp = crate::app::Comp::<C>::new(comp);
        comp.pre_render();
        let dom = comp.render_in(self.ws_node());
        comp.post_render();

        self.children
            .0
            .lock()
            .expect_throw("TotalElement::component: self.children.lock()")
            .push(dom.into());
    }

    fn component_signal<S>(&mut self, signal: S)
    where
        S: 'static + signals::signal::Signal<Item = Box<dyn crate::app::FlowContentComponent>>,
    {
        let active_comp = std::sync::Arc::new(std::sync::Mutex::new(None));
        let parent_node = self.ws_node().clone();

        let temp_node = TotalElement::with_tag("span");
        temp_node.append_to(&parent_node);

        let mut current_node = temp_node.ws_node().clone();

        self.spawn_for_each(signal, move |mut comp| {
            let mut active_comp = active_comp
                .lock()
                .expect_throw("NonEmptyElement::component_signal: active_comp.lock()");
            if let Some(_old_app) = active_comp.take() {
                // call un_mount on old_app if it is introduced
            }
            comp.pre_render();
            let dom = comp.render_and_replace(&parent_node, &mut current_node);
            comp.post_render();
            active_comp.replace(ActiveComponent {
                _comp: comp,
                _dom: dom,
            });
        });
    }

    fn custom_element<C, E>(&mut self, comp: C)
    where
        C: 'static + crate::app::CustomElement<Container = E>,
        E: 'static + FlowContent,
    {
        use crate::app::FlowContentCustomElement;

        let comp = Box::new(comp) as Box<dyn FlowContentCustomElement>;
        comp.append_to(self.ws_node());
        comp.mounted();
        self.children
            .0
            .lock()
            .expect_throw("NonEmptyElement::custom_element: self.children.lock()")
            .push(Node::CustomElement(comp));
    }
}

impl ChildOfTable for ClosedElement<Tbody> {}

pub struct Body(TotalElement);

impl From<Body> for TotalElement {
    fn from(item: Body) -> Self {
        item.0
    }
}

impl AsMut<TotalElement> for Body {
    fn as_mut(&mut self) -> &mut TotalElement {
        &mut self.0
    }
}

impl AllowText for Body {}

// Just for this (the Body) to be able to pass to App::mount_to_body().
impl HasWsNode for Body {
    fn ws_node(&self) -> &web_sys::Node {
        self.0.ws_node()
    }
}

impl StoreHandle for Body {
    fn store_future_handle(
        &mut self,
        handle: discard::DiscardOnDrop<signals::CancelableFutureHandle>,
    ) {
        self.0.store_future_handle(handle);
    }

    fn store_listener(&mut self, listener: Box<dyn crate::events::Listener>) {
        self.0.store_listener(listener);
    }
}

impl AllowComponent<Body> for Body {}

impl Body {
    pub fn get() -> Self {
        Self(TotalElement::with_ws_element(
            crate::document()
                .body()
                .expect_throw("Body::get: crate::document().body()")
                .into(),
        ))
    }

    pub fn clear(self) -> Self {
        self.0.ws_node().set_text_content(None);
        self
    }

    pub fn child<T: Into<TotalElement> + FlowContent>(self, child: T) -> Self {
        self.0.child(child);
        self
    }

    pub fn child_signal<S, T>(mut self, signal: S) -> Self
    where
        T: 'static + Into<TotalElement> + FlowContent,
        S: 'static + signals::signal::Signal<Item = T>,
    {
        self.0.child_signal(signal);
        self
    }

    // pub fn custom_element<C, E>(mut self, comp: C) -> Self
    // where
    //     C: 'static + crate::app::CustomElement<Container = E>,
    //     E: 'static + FlowContent,
    // {
    //     self.0.custom_element(comp);
    //     self
    // }
}

pub trait MountableToBody {
    fn is_body(&self) -> bool;
}

impl<T: FlowContent> MountableToBody for T {
    fn is_body(&self) -> bool {
        false
    }
}
impl MountableToBody for Body {
    fn is_body(&self) -> bool {
        true
    }
}

pub struct Text {
    ws_node: web_sys::Node,
    text: String,
}

impl Text {
    pub fn with_node_and_text(ws_node: web_sys::Node, text: &str) -> Self {
        ws_node.set_text_content(Some(text));
        Self {
            ws_node,
            text: text.to_string(),
        }
    }

    pub fn ws_node(&self) -> &web_sys::Node {
        &self.ws_node
    }

    pub fn text(&mut self, text: &str) {
        if self.text != text {
            self.text = text.to_string();
            self.ws_node.set_text_content(Some(text));
        }
    }
}

impl From<Text> for Node {
    fn from(text: Text) -> Self {
        Node::Text(text)
    }
}

struct ActiveComponent<T> {
    _comp: T,
    _dom: TotalElement,
}

/// This struct is the output of `list`, `list_signal`, `tab_components`. No more child can be added to this element.
pub struct ClosedElement<T>(T);

impl<T> ClosedElement<T> {
    fn new(t: T) -> Self {
        Self(t)
    }
}

impl<T: HasWsNode> HasWsNode for ClosedElement<T> {
    fn ws_node(&self) -> &web_sys::Node {
        self.0.ws_node()
    }
}

define_helper_enums! {
    ButtonType {
        Button => "button",
        Submit => "submit",
        Reset => "reset",
    }
    InputType {
        Button => "button",
        CheckBox => "checkbox",
        Color => "color",
        Date => "date",
        DateTimeLocal => "datetime-local",
        Email => "email",
        FileUpload => "file",
        Hidden => "hidden",
        ImageButton => "image",
        Month => "month",
        Number => "number",
        Password => "password",
        RadioButton => "radio",
        Range => "range",
        ResetButton => "reset",
        Search => "search",
        SubmitButton => "submit",
        Telephone => "tel",
        Text => "text",
        Time => "time",
        Url => "url",
        Week => "week",
    }
    AutoCapitalize {
        Off => "off",
        On => "on",
        Sentences => "sentences",
        Words => "words",
        Characters => "characters",
    }
    Dir {
        LeftToRight => "ltr",
        RightToLeft => "rtl",
        Auto => "auto",
    }
    InputMode {
        None => "none",
        Text => "text",
        Decimal => "decimal",
        Numeric => "numeric",
        Tel => "tel",
        Search => "search",
        Email => "email",
        Url => "url",
    }
    Target {
        _Self => "_self",
        #[deprecated(note = "There is a Security_and_privacy_concerns when `target='_blank'`, please use `.target_blank_with_rel()`. See more at https://developer.mozilla.org/en-US/docs/Web/HTML/Element/a#Security_and_privacy_concerns")]
        _Blank => "_blank",
        _Parent => "_parent",
        _Top => "_top",
    }
    PreLoad {
        None => "none",
        MetaData => "metadata",
        Auto => "auto",
    }
    EncType {
        ComponentXWwwFormUrlEncoded => "Component/x-www-form-urlencoded",
        MultiPartFormData => "multipart/form-data",
        TextPlain => "text/plain",
    }
    FormMethod {
        Post => "post",
        Get => "get",
    }
    AutoComplete {
        On => "on",
        Off => "off",
    }
    ReferrerPolicy {
        NoReferrer => "no-referrer",
        NoReferrerWhenDowngrade => "no-referrer-when-downgrade",
        Origin => "origin",
        OriginWhenCrossOrigin => "origin-when-cross-origin",
        SameOrigin => "same-origin",
        StrictOrigin => "strict-origin",
        StrictOriginWhenCrossOrigin => "strict-origin-when-cross-origin",
        UnsafeUrl => "unsafe-url",
    }
    Sandbox {
        AllowForms => "allow-forms",
        AllowModals => "allow-modals",
        AllowOrientationLock => "allow-orientation-lock",
        AllowPointerLock => "allow-pointer-lock",
        AllowPopups => "allow-popups",
        AllowPopupsToEscapeSandbox => "allow-popups-to-escape-sandbox",
        AllowPresentation => "allow-presentation",
        AllowSameOrigin => "allow-same-origin",
        AllowScripts => "allow-scripts",
        // Should we have feature = "experimental" for things like this?
        // allow-storage-access-by-user-activation
        AllowTopNavigation => "allow-top-navigation",
        AllowTopNavigationByUserActivation => "allow-top-navigation-by-user-activation",
        //AllowDownloadsWithoutUserActivation => "allow-downloads-without-user-activation",
    }
    CrossOrigin {
        Anonymous => "anonymous",
        UseCredentials => "use-credentials",
    }
    Decoding {
        Sync => "sync",
        Async => "async",
        Auto => "auto",
    }
    OlType {
        Number => "1",
        LowerCase => "a",
        UpperCase => "C",
        LowerCaseRoman => "i",
        UpperCaseRoman => "I",
    }
    SpellCheck {
        True => "true",
        False => "false",
        Default => "default",
    }
    Wrap {
        Hard => "hard",
        Soft => "soft",
    }
    ThScope {
        Row => "row",
        Col => "col",
        RowGroup => "rowgroup",
        ColGroup => "colgroup",
        Auto => "auto",
    }
    TrackKind {
        Subtitles => "subtitles",
        Captions => "captions",
        Descriptions => "descriptions",
        Chapters => "chapters",
        Metadata => "metadata",
    }
}

/// If you don't want to use this struct, construct your own
/// string and set it by .bool_attribute("coords", ...)
pub enum Coords {
    Rect {
        left: i32,
        top: i32,
        right: i32,
        bottom: i32,
    },
    Circle {
        x: i32,
        y: i32,
        radius: i32,
    },
    // TODO a good way to represent a polygon?
    Poly(String),
}

define_helper_enums! {
    LinkTypeForTargetBlank {
        NoReferrer => "noreferrer",
        NoReferrerNoOpener => "noreferrer noopener",
    }
}

impl TotalElement {
    pub fn target_blank_with_rel(&self, rel: LinkTypeForTargetBlank) {
        self.attribute_string("target", "_blank");
        self.attribute_string("rel", rel.as_str())
    }
}

impl A {
    /// Because of [Security_and_privacy_concerns](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/a#Security_and_privacy_concerns),
    /// Mika deprecated `Target::_Blank` and provides this method for setting both `target=_'blank'` and `rel='noreferrer'` together.
    // TODO:
    // Currently, in the developing version, this is just a demonstration for `How Mika will try to help`.
    // We are not sure if this implementation is correct or not. This needs reviews from experienced users or
    // some experiments to conclude.

    // TODO: How to replicate this for all affected elements (<a>, <area>, <button>, <form>) without copy/paste: trait?, macro?
    pub fn target_blank_with_rel(self, rel: LinkTypeForTargetBlank) -> Self {
        self.0.target_blank_with_rel(rel);
        self
    }
}

impl TotalElement {
    pub fn coords(&self, value: &Coords) {
        let (shape, coords) = match value {
            Coords::Rect {
                left,
                top,
                right,
                bottom,
            } => ("rect", format!("{},{},{},{}", left, top, right, bottom)),
            Coords::Circle { x, y, radius } => ("circle", format!("{},{},{}", x, y, radius)),
            Coords::Poly(s) => ("rect", s.clone()),
        };
        self.attribute_string("shape", shape);
        self.attribute_string("coords", &coords);
    }
}

impl Area {
    pub fn coords(self, value: &Coords) -> Self {
        self.0.coords(value);
        self
    }
}

impl TotalElement {
    pub fn sandbox(&self, allows: &[Sandbox]) {
        self.attribute_string(
            "sandbox",
            &allows
                .iter()
                .map(|i| i.as_str())
                // TODO how to avoid collect?
                .collect::<Vec<&str>>()
                .join(" "),
        );
    }
}

impl Iframe {
    pub fn sandbox(self, allows: &[Sandbox]) -> Self {
        self.0.sandbox(allows);
        self
    }
}

impl TotalElement {
    pub fn checked(&self, value: bool) {
        self.ws_element
            .unchecked_ref::<web_sys::HtmlInputElement>()
            .set_checked(value);
    }

    pub fn checked_signal<S>(&mut self, signal: S)
    where
        S: 'static + signals::signal::Signal<Item = bool>,
    {
        let ws_input: web_sys::HtmlInputElement = self.ws_element.clone().unchecked_into();
        self.spawn_for_each(signal, move |value| {
            ws_input.set_checked(value);
        });
    }
}

impl Input {
    pub fn checked(self, value: bool) -> Self {
        self.0.checked(value);
        self
    }

    pub fn checked_signal<S>(mut self, signal: S) -> Self
    where
        S: 'static + signals::signal::Signal<Item = bool>,
    {
        self.0.checked_signal(signal);
        self
    }
}

impl TotalElement {
    pub fn selected_index(&self, value: std::option::Option<usize>) {
        let ws_select: &web_sys::HtmlSelectElement = self.ws_element.unchecked_ref();
        match value {
            Some(value) => ws_select.set_selected_index(value as i32),
            None => ws_select.set_selected_index(-1),
        }
    }

    pub fn selected_index_signal<S>(&mut self, signal: S)
    where
        S: 'static + signals::signal::Signal<Item = std::option::Option<usize>>,
    {
        let ws_select: web_sys::HtmlSelectElement = self.ws_element.clone().unchecked_into();
        self.spawn_for_each(signal, move |value| match value {
            Some(value) => ws_select.set_selected_index(value as i32),
            None => ws_select.set_selected_index(-1),
        });
    }

    pub fn value(&self, value: std::option::Option<&str>) {
        let ws_select: &web_sys::HtmlSelectElement = self.ws_element.unchecked_ref();
        match value {
            Some(value) => ws_select.set_value(value),
            None => ws_select.set_selected_index(-1),
        }
    }

    pub fn value_signal<S>(&mut self, signal: S)
    where
        S: 'static + signals::signal::Signal<Item = std::option::Option<String>>,
    {
        let ws_select: web_sys::HtmlSelectElement = self.ws_element.clone().unchecked_into();
        self.spawn_for_each(signal, move |value| match value {
            Some(value) => ws_select.set_value(&value),
            None => ws_select.set_selected_index(-1),
        });
    }
}

impl Select {
    pub fn selected_index(self, value: std::option::Option<usize>) -> Self {
        self.0.selected_index(value);
        self
    }

    pub fn selected_index_signal<S>(mut self, signal: S) -> Self
    where
        S: 'static + signals::signal::Signal<Item = std::option::Option<usize>>,
    {
        self.0.selected_index_signal(signal);
        self
    }

    pub fn value(self, value: std::option::Option<&str>) -> Self {
        self.0.value(value);
        self
    }

    pub fn value_signal<S>(mut self, signal: S) -> Self
    where
        S: 'static + signals::signal::Signal<Item = std::option::Option<String>>,
    {
        self.0.value_signal(signal);
        self
    }
}

struct KeyedElement<K, E, U> {
    key: K,
    element: E,
    to_update: U,
}

impl<K, E, U> KeyedElement<K, E, U> {
    fn new<C, I>(item: &I, render_output: (E, U)) -> Self
    where
        C: crate::app::Component,
        I: crate::list::KeyedItem<C> + crate::list::Key<Key = K>,
        K: Clone + Eq + std::hash::Hash,
    {
        Self {
            key: item.key().clone(),
            element: render_output.0,
            to_update: render_output.1,
        }
    }
}
