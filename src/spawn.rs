use signals::signal::SignalExt;
use signals::signal_vec::{SignalVecExt, VecDiff};

// Most of these are from dominator

fn spawn<F>(future: F) -> discard::DiscardOnDrop<signals::CancelableFutureHandle>
where
    F: 'static + std::future::Future<Output = ()>,
{
    let (handle, future) = signals::cancelable_future(future, || ());
    wasm_bindgen_futures::spawn_local(future);
    handle
}

pub fn for_each<S, F>(
    signal: S,
    mut f: F,
) -> discard::DiscardOnDrop<signals::CancelableFutureHandle>
where
    S: 'static + signals::signal::Signal,
    F: 'static + FnMut(S::Item),
{
    let future = signal.for_each(move |value| {
        f(value);
        futures_util::future::ready(())
    });
    spawn(future)
}

// TODO: Store all future from signal and stop them when require
pub fn for_each_vec<S, F>(
    signal: S,
    mut f: F,
) -> discard::DiscardOnDrop<signals::CancelableFutureHandle>
where
    S: 'static + signals::signal_vec::SignalVec,
    F: 'static + FnMut(VecDiff<S::Item>),
{
    let future = signal.for_each(move |value| {
        f(value);
        futures_util::future::ready(())
    });
    spawn(future)
}

// TODO: Better name
#[derive(Default)]
pub struct SignalCenter {
    handles: Vec<discard::DiscardOnDrop<signals::CancelableFutureHandle>>,
}

impl SignalCenter {
    pub fn new() -> Self {
        Self {
            handles: Vec::new(),
        }
    }
    pub fn for_each<S, F>(&mut self, signal: S, mut f: F)
    where
        S: 'static + signals::signal::Signal,
        F: 'static + FnMut(S::Item),
    {
        let future = signal.for_each(move |value| {
            f(value);
            futures_util::future::ready(())
        });
        self.handles.push(spawn(future));
    }
}
