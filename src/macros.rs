#[macro_export]
macro_rules! handler {

    ($comp:expr, $($method_path:ident)::+) => {
        $crate::callbacks::event_handlers::handler_no_arg($comp, $($method_path)::+)
    };

    ($comp:expr, $($method_path:ident)::+()) => {
        $crate::callbacks::event_handlers::handler_no_arg($comp, $($method_path)::+)
    };

    ($comp:expr, $($method_path:ident)::+(:arg)) => {
        $crate::callbacks::event_handlers::handler_arg($comp, $($method_path)::+)
    };

    // ($comp:expr, $($method_path:ident)::+(:context)) => {
    //     $crate::callbacks::event_handlers::handler_context($comp, $($method_path)::+)
    // };

    // ($comp:expr, $($method_path:ident)::+(:event,:context)) => {
    //     $crate::callbacks::event_handlers::handler_arg_context($comp, $($method_path)::+)
    // };

    ($comp:expr, $($method_path:ident)::+($($tt:tt)*)) => {
        $crate::callbacks::event_handlers::handler_no_arg($comp, move |__comp__state__| $($method_path)::+(__comp__state__, $($tt)*))
    };
}

#[macro_export]
macro_rules! callback {
    ($comp:expr, $($method_path:ident)::+) => {
        $crate::callbacks::callback_no_arg($comp, $($method_path)::+)
    };

    ($comp:expr, $($method_path:ident)::+()) => {
        $crate::callbacks::callback_no_arg($comp, $($method_path)::+)
    };

    ($comp:expr, $($method_path:ident)::+(:arg)) => {
        $crate::callbacks::callback_arg($comp, $($method_path)::+)
    };

    // ($comp:expr, $($method_path:ident)::+(:context)) => {
    //     $crate::callbacks::callback_context($comp, $($method_path)::+)
    // };

    // ($comp:expr, $($method_path:ident)::+(:arg,:context)) => {
    //     $crate::callbacks::callback_arg_context($comp, $($method_path)::+)
    // };

    ($comp:expr, $($method_path:ident)::+($($tt:tt)*)) => {
        $crate::callbacks::callback_no_arg($comp, move |__comp__state__| $($method_path)::+(__comp__state__, $($tt)*))
    };
}

/// Create an event handler from a method call on a RawComponent state.
/// 'raw_handler!` will clone the state and create a moved closure that
/// wraps your state's method call. The result is intended to be passed
/// to an event register method such as `.on_click`.
///
/// ## Example
/// ```rust
/// struct AppState;
/// impl AppState {
///     fn do_something(&self) { }
/// }
/// impl mika::RawComponent for AppState {
///     type RenderOutput = mika::dom::Button;
///     fn render(app: &std::rc::Rc<Self>) -> Self::RenderOutput {
///         mika::dom::Button::new()
///             .on_click(
///                 mika::raw_handler!{ app.do_something() }
///             )
///     }
/// }
/// ```
/// If the RawComponent's state method want to receive the event argument, it must be the first
/// argument of the method. Then, you call `raw_handler!` with the event argument
/// placeholder: `_`.
/// ## Example
/// ```rust
/// impl AppState {
///     fn do_with_event_arg(&self, event: web_sys::Event) {
///         // Do something with `event`
///     }
/// }
/// impl mika::RawComponent for AppState {
///     type RenderOutput = mika::dom::Input;
///     fn render(app: &std::rc::Rc<Self>) -> Self::RenderOutput {
///         mika::dom::Input::new()
///             .on_change(
///                 mika::raw_handler!{ app.do_with_event_arg(_) }
///             )
///     }
/// }
/// ```
/// If the method wants an extra argument, the you must make sure the argument to be
/// able to be moved into the handler (usually, it must not a reference).
/// ## Example
/// ```rust
/// impl AppState {
///     fn do_with_event_arg_and_extra_arg(&self, event: web_sys::Event, extra: i32) {
///         // Do something with `event` and `extra`
///     }
/// }
/// impl mika::RawComponent for AppState {
///     type RenderOutput = mika::dom::Input;
///     fn render(app: &std::rc::Rc<Self>) -> Self::RenderOutput {
///         let the_extra_value = 42;
///         mika::dom::Input::new()
///             .on_change(
///                 mika::raw_handler!{ app.do_with_event_arg_and_extra_arg(_, the_extra_value) }
///             )
///     }
/// }
/// ```

#[macro_export]
macro_rules! raw_handler {
    ($state:ident . $($idents:ident).+ (_)) => {{
        // replace all these with: std::rc::Rc::clone?
        let $state = $state.clone();
        move |arg| $state.$($idents).+(arg)
    }};
    ($state:ident . $($idents:ident).+ (_, $($tt:tt)+)) => {{
        let $state = $state.clone();
        move |arg| $state.$($idents).+(arg, $($tt)+)
    }};
    ($state:ident . $($idents:ident).+ ($($tt:tt)*)) => {{
        let $state = $state.clone();
        move |_| $state.$($idents).+($($tt)*)
    }};
}

/// This is about to be removed. Don't use this
#[macro_export]
#[deprecated(
    note = "Try other way to implement a context. Currently tries to experiment with StatelessComponent, Commands (for Component only)"
)]
macro_rules! raw_context_callback {
    ($state:ident . $context:ident . $context_method_name:ident ($($arg_name:ident: $ArgType:ty),*)) => {{
        let $state = $state.clone();
        let handler = move | $($arg_name: $ArgType),* | {
            // TODO: try_borrow_mut ?
            $state.$context.borrow_mut().$context_method_name($($arg_name),*);
        };
        wasm_bindgen::closure::Closure::wrap(Box::new(handler) as Box<Fn($($ArgType),*)>)
    }};
}

/// Similar in functionality to `raw_handler!`. But the resulted handler will call to
/// a special RawComponent-state's method named `update`. The `update` method must receive a
/// single argument, which is an enum.
/// ## Example
/// ```rust
/// struct AppState;
/// enum Msg {
///     Foo,
///     Bar(web_sys::Event),
///     Baz(u32),
/// }
/// impl AppState {
///     fn update(&self, m: Msg) {
///         // Do something with `m`
///     }
/// }
/// impl mika::RawComponent for AppState {
///     type RenderOutput = mika::dom::Button;
///     fn render(app: &std::rc::Rc<Self>) -> Self::RenderOutput {
///         let id = 42;
///         mika::dom::Input::new()
///             .on_change(
///                 mika::update!{ app, Msg::Foo }
///                 // or mika::update!{ app, Msg::Bar(_) }
///                 // or mika::update!{ app, Msg::Baz(id) }
///             )
///     }
/// }
/// ```

#[macro_export]
macro_rules! raw_update {
    ($state:ident, $first:ident$(::$idents:ident)*) => {{
        // replace all these with: std::rc::Rc::clone?
        let $state = $state.clone();
        move |_| $state.update($first$(::$idents)*)
    }};
    ($state:ident, $first:ident$(::$idents:ident)*(_)) => {{
        let $state = $state.clone();
        move |arg| $state.update($first$(::$idents)*(arg))
    }};
    ($state:ident, $first:ident$(::$idents:ident)*(_, $($tt:tt)+)) => {{
        let $state = $state.clone();
        move |arg| $state.update($first$(::$idents)*(arg, $($tt)+))
    }};
    ($state:ident, $first:ident$(::$idents:ident)*($($tt:tt)+)) => {{
        let $state = $state.clone();
        move |_| $state.update($first$(::$idents)*($($tt)+))
    }};
}

/// Create a handle for your app named `AppHandle`. The app's handle will be export by #[wasm-bindgen].
/// ```rust
/// struct AppState {}
/// impl Component for AppState {
///     // ...
/// }
/// // This will mount your app in the document.body
/// mika::create_app_handle!(AppState);
/// // Or, this will mount your app in a `<div>` that has `id="div-id"`
/// mika::create_app_handle!(AppState, Div("div-id"));
/// ```
/// In JavaScript, you can start your app by construct a new instance of AppHandle:
/// ```
/// window.my_app = new AppHandle();
/// ```
/// then, your app will be up and run.
#[macro_export]
macro_rules! impl_create_app_handle {
    ($AppType:ident, $TypeName:ty) => {
        #[wasm_bindgen]
        pub struct AppHandle {
            _app: $crate::$AppType<$TypeName>,
        }

        #[allow(clippy::new_without_default)]
        #[wasm_bindgen]
        impl AppHandle {
            #[wasm_bindgen(constructor)]
            pub fn new() -> Self {
                Self {
                    _app: $crate::$AppType::mount_to_body(Default::default()),
                }
            }
        }
    };
    ($AppType:ident, $TypeName:ty, $ElementType:ident($element_id:literal)) => {
        #[wasm_bindgen]
        pub struct AppHandle {
            _app: $crate::$AppType<$TypeName>,
        }

        #[allow(clippy::new_without_default)]
        #[wasm_bindgen]
        impl AppHandle {
            #[wasm_bindgen(constructor)]
            pub fn new() -> Self {
                Self {
                    _app: $crate::$AppType::mount_to(
                        Default::default(),
                        &$crate::dom::$ElementType::with_id($element_id)
                            .expect_throw(concat!("No element id = ", $element_id)),
                    ),
                }
            }
        }
    };
}

#[macro_export]
macro_rules! create_raw_app_handle {
    ($($tt:tt)+) => { $crate::impl_create_app_handle!{ RawApp, $($tt)+ } }
}

#[macro_export]
macro_rules! create_app_handle {
    ($($tt:tt)+) => { $crate::impl_create_app_handle!{ App, $($tt)+ } }
}

macro_rules! define_helper_enums {
    ($(
        $(#[$enum_meta_content:meta])*
        $EnumTypeName:ident { $(
            $(#[$variant_meta_content:meta])*
            $VariantName:ident => $str_value:literal,
        )+}
    )+) => {
        $(
            $(#[$enum_meta_content])*
            pub enum $EnumTypeName {
                $(
                    $(#[$variant_meta_content])*
                    $VariantName,
                )+
            }
            impl $EnumTypeName {
                fn as_str(&self) -> &'static str {
                    match self {
                        $(
                            #[allow(deprecated)]
                            $EnumTypeName::$VariantName => $str_value,
                        )+
                    }
                }
            }
        )+
    };
}

#[cfg(test)]
mod test {
    enum Msg {
        N42,
        Double(i32),
        Add(i32, i32),
        Sum(i32, i32, i32),
    }
    #[derive(Clone)]
    struct Test;
    impl Test {
        fn n42(&self) -> i32 {
            42
        }
        fn double(&self, me: i32) -> i32 {
            me * 2
        }
        fn add(&self, a: i32, b: i32) -> i32 {
            a + b
        }
        fn sum(&self, a: i32, b: i32, c: i32) -> i32 {
            a + b + c
        }
        fn update(&self, m: Msg) -> i32 {
            match m {
                Msg::N42 => 42,
                Msg::Double(value) => value * 2,
                Msg::Add(v1, v2) => v1 + v2,
                Msg::Sum(v1, v2, v3) => v1 + v2 + v3,
            }
        }
    }

    #[test]
    fn handler_no_arg() {
        assert_eq!(42, raw_handler!(Test.n42())(()));
    }

    #[test]
    fn handler_with_arg() {
        assert_eq!(84, raw_handler!(Test.double(42))(()));
        assert_eq!(84, raw_handler!(Test.add(42, 42))(()));
        assert_eq!(85, raw_handler!(Test.sum(42, 42, 1))(()));
    }

    #[test]
    fn handler_optional_arg_only() {
        assert_eq!(84, raw_handler!(Test.double(_))(42));
    }

    #[test]
    fn handler_optional_arg_and_other() {
        assert_eq!(84, raw_handler!(Test.add(_,42))(42));
        assert_eq!(85, raw_handler!(Test.sum(_,42,1))(42));
    }

    #[test]
    fn update() {
        assert_eq!(42, raw_update!(Test, Msg::N42)(()));
        assert_eq!(84, raw_update!(Test, Msg::Double(42))(()));
        assert_eq!(84, raw_update!(Test, Msg::Add(42, 42))(()));
        assert_eq!(80, raw_update!(Test, Msg::Sum(42, 42, -4))(()));
    }

    #[test]
    fn update_with_optional_arg() {
        assert_eq!(84, raw_update!(Test, Msg::Double(_))(42));
        assert_eq!(84, raw_update!(Test, Msg::Add(_, 42))(42));
        assert_eq!(80, raw_update!(Test, Msg::Sum(_, 42, 42))(-4));
    }
}
