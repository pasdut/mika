pub fn serde<T, F>(handler: F) -> wasm_bindgen::closure::Closure<dyn Fn(wasm_bindgen::JsValue)>
where
    T: serde::de::DeserializeOwned,
    F: 'static + Fn(Result<T, crate::Error>),
{
    let handler = move |value: wasm_bindgen::JsValue| {
        let value: Result<T, serde_json::Error> = value.into_serde();
        handler(value.map_err(From::from));
    };
    wasm_bindgen::closure::Closure::wrap(Box::new(handler) as Box<dyn Fn(wasm_bindgen::JsValue)>)
}

pub fn serde_ok_error<T, F1, F2>(
    ok_handler: F1,
    error_handler: F2,
) -> wasm_bindgen::closure::Closure<dyn Fn(wasm_bindgen::JsValue)>
where
    T: serde::de::DeserializeOwned,
    F1: 'static + Fn(T),
    F2: 'static + Fn(crate::Error),
{
    let handler = move |value: wasm_bindgen::JsValue| {
        let value: Result<T, serde_json::Error> = value.into_serde();
        match value {
            Ok(data) => ok_handler(data),
            Err(e) => error_handler(From::from(e)),
        }
    };
    wasm_bindgen::closure::Closure::wrap(Box::new(handler) as Box<dyn Fn(wasm_bindgen::JsValue)>)
}
