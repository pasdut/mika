//! Help creating event handlers, use by element events such as on_click, on_change...
//! `mika::handler!` call these for you.

pub fn handler_no_arg<C, F, T, Ct>(comp_handle: &crate::app::CompHandle<C>, f: F) -> impl Fn(T)
where
    C: 'static + crate::app::Component,
    F: Fn(&mut C::State) -> Ct,
    Ct: Into<crate::app::Context<C>>,
{
    let comp_handle = comp_handle.clone();
    move |_: T| comp_handle.update(&f)
}

pub fn handler_arg<C, F, T, Ct>(comp_handle: &crate::app::CompHandle<C>, f: F) -> impl Fn(T)
where
    C: 'static + crate::app::Component,
    F: Fn(&mut C::State, T) -> Ct,
    Ct: Into<crate::app::Context<C>>,
{
    let comp_handle = comp_handle.clone();
    move |arg: T| comp_handle.update_with_arg(arg, &f)
}

// pub fn handler_context<C, F, T>(comp_handle: &crate::app::CompHandle<C>, f: F) -> impl Fn(T)
// where
//     C: 'static + crate::app::Component,
//     F: Fn(&mut C::State, &mut crate::app::Context<C>),
// {
//     let comp_handle = comp_handle.clone();
//     move |_: T| comp_handle.update_with_context(&f)
// }

// pub fn handler_arg_context<C, F, T>(comp_handle: &crate::app::CompHandle<C>, f: F) -> impl Fn(T)
// where
//     C: 'static + crate::app::Component,
//     F: Fn(&mut C::State, T, &mut crate::app::Context<C>),
// {
//     let comp_handle = comp_handle.clone();
//     move |arg: T| comp_handle.update_with_arg_and_context(arg, &f)
// }
