#[derive(Debug)]
pub enum Error {
    ElementByIdNotFound(String),
    WrongTag {
        expected_tag: String,
        found_tag: String,
    },
    FetchError(String),
    JsonError(serde_json::Error, String),
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::ElementByIdNotFound(id) => {
                write!(f, "No element: Element id='{}' not found", id)
            }
            Error::WrongTag {
                expected_tag,
                found_tag,
            } => write!(
                f,
                "Wrong element: expected tag: `{}`, but found: `{}`",
                expected_tag, found_tag
            ),
            Error::FetchError(s) => write!(f, "Fetch error: {}", s),
            Error::JsonError(e, s) => write!(f, "Json error: {}. Data: {}", e, s),
        }
    }
}

impl From<serde_json::Error> for Error {
    fn from(e: serde_json::Error) -> Self {
        Error::JsonError(e, "[Unknown because it is From<serde_json::Error>]".to_string())
    }
}
