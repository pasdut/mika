use futures_util::future::FutureExt;
use futures_util::TryFutureExt;

use wasm_bindgen::UnwrapThrowExt;

define_helper_enums! {
    Method {
        Get => "GET",
        Head => "HEAD",
        Post => "POST",
        Put => "PUT",
        Delete => "DELETE",
        Connect => "CONNECT",
        Options => "OPTIONS",
        Trace => "TRACE",
        Patch => "PATCH",
    }
    /// https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers
    Header {
        Accept => "Accept",
        ContentType => "Content-Type",
    }
}

pub struct Request {
    url: String,
    request_init: web_sys::RequestInit,
    headers: Option<web_sys::Headers>,
}

impl Request {
    pub fn new(method: Method, url: &str) -> Self {
        let mut r = Self {
            url: url.to_string(),
            request_init: web_sys::RequestInit::new(),
            headers: None,
        };
        r.request_init.method(method.as_str());
        r
    }

    pub fn get(url: &str) -> Self {
        Self::new(Method::Get, url)
    }

    pub fn post(url: &str) -> Self {
        Self::new(Method::Post, url)
    }

    pub fn set_header(&mut self, name: Header, val: &str) {
        let headers = self.headers.get_or_insert_with(|| {
            web_sys::Headers::new().expect_throw("Request::header: web_sys::Headers::new()")
        });
        headers
            .set(name.as_str(), val)
            .expect_throw("Request::header: headers.set(name,val)");
    }

    // TODO: Define helper types for header's values.
    pub fn header(mut self, name: Header, val: &str) -> Self {
        self.set_header(name, val);
        self
    }

    pub fn body_json<S: serde::Serialize>(&mut self, data: &S) -> Result<(), crate::Error> {
        self.set_header(Header::ContentType, "application/json");
        self.request_init
            .body(Some(&wasm_bindgen::JsValue::from(serde_json::to_string(
                data,
            )?)));
        Ok(())
    }

    /// This will execute fetch and try to deserialize the received data as a JSON text.
    /// And send the result back to your app via the given handler. Your handler must
    /// accept `Result<T, mika::errors::Error>`.
    /// ## Example:
    /// ```rust
    /// struct AppState {}
    /// impl AppState {
    ///     fn fetched_result(&self, result: Result<DeserializableData, mika::errors::Error>) {}
    ///
    ///     fn start_fetching(&self, app: &std::rc::Rc<Self>) {
    ///         mika::fetch::Request::new(mika::fetch::Method::Get, "some/url")
    ///             .fetch_json(mika::handler!(app.fetched_result(_)));
    ///     }
    /// }
    /// ```
    pub fn fetch_json<T, F>(mut self, handler: F)
    where
        T: serde::de::DeserializeOwned,
        F: 'static + FnOnce(Result<T, crate::Error>),
    {
        use wasm_bindgen_futures::JsFuture;
        if let Some(headers) = self.headers {
            self.request_init.headers(&headers);
        }
        let promise = crate::window().fetch_with_str_and_init(&self.url, &self.request_init);
        let future = JsFuture::from(promise)
            .map(|rs| match rs {
                Ok(rs) => match web_sys::Response::from(rs).text() {
                    Ok(text) => Ok(text),
                    Err(e) => Err(e),
                },
                Err(e) => Err(e),
            })
            .and_then(JsFuture::from)
            .map(|rs| match rs {
                Err(e) => handler(Err(crate::Error::FetchError(
                    e.as_string().unwrap_or_else(|| "[Unknown error]".to_string()),
                ))),
                Ok(text) => match text.as_string() {
                    None => handler(Err(crate::Error::FetchError("No data".to_string()))),
                    Some(text) => {
                        match serde_json::from_str::<T>(&text) {
                            Ok(data) => handler(Ok(data)),
                            Err(e) => handler(Err(crate::Error::JsonError(e, text))),
                        };
                    }
                },
            });
        wasm_bindgen_futures::spawn_local(future);
    }

    /// Works similar to `fetch_json`. The difference is that you want a handler for Ok result, another one to handle Err result.
    pub fn fetch_json_ok_error<T, F1, F2>(mut self, ok_handler: F1, error_handler: F2)
    where
        T: serde::de::DeserializeOwned,
        F1: 'static + FnOnce(T),
        F2: 'static + FnOnce(crate::Error),
    {
        use wasm_bindgen_futures::JsFuture;
        if let Some(headers) = self.headers {
            self.request_init.headers(&headers);
        }
        let promise = crate::window().fetch_with_str_and_init(&self.url, &self.request_init);
        let future = JsFuture::from(promise)
            .map(|rs| match rs {
                Ok(rs) => {
                    let rs = web_sys::Response::from(rs);
                    if rs.ok() {
                        match rs.text() {
                            Ok(text) => Ok(text),
                            Err(e) => Err(e),
                        }
                    }
                    else {
                        Err(rs.status_text().into())
                    }
                },
                Err(e) => Err(e),
            })
            .and_then(JsFuture::from)
            .map(|rs| match rs {
                Err(e) => error_handler(crate::Error::FetchError(
                    e.as_string().unwrap_or_else(|| "[Unknown error]".to_string()),
                )),
                Ok(text) => match text.as_string() {
                    None => error_handler(crate::Error::FetchError("No data".to_string())),
                    Some(text) => {
                        match serde_json::from_str::<T>(&text) {
                            Ok(data) => ok_handler(data),
                            Err(e) => error_handler(crate::Error::JsonError(e, text)),
                        };
                    }
                },
            });
        wasm_bindgen_futures::spawn_local(future);
    }
}

struct FetchCmdArgs<C, F>
where
    C: crate::app::Component,
{
    request: Request,
    ok_handler: F,
    error_handler: fn(&mut C::State, crate::Error),
}

pub struct FetchCommand<C, F, T>
where
    C: crate::app::Component,
{
    args: Option<FetchCmdArgs<C, F>>,
    _phantom_data: std::marker::PhantomData<T>,
}

impl<C, F, T> FetchCommand<C, F, T>
where
    C: crate::app::Component,
{
    pub(crate) fn new(
        request: Request,
        ok_handler: F,
        error_handler: fn(&mut C::State, crate::Error),
    ) -> Self {
        Self {
            args: Some(FetchCmdArgs {
                request,
                ok_handler,
                error_handler,
            }),
            _phantom_data: std::marker::PhantomData,
        }
    }
}

impl<C, F, T> crate::app::Command<C> for FetchCommand<C, F, T>
where
    C: 'static + crate::app::Component,
    T: 'static + serde::de::DeserializeOwned,
    F: 'static + Fn(&mut C::State, T),
{
    fn execute(&mut self, comp: &crate::app::CompHandle<C>) {
        let FetchCmdArgs {
            request,
            ok_handler,
            error_handler,
        } = self.args.take().unwrap();
        request.fetch_json_ok_error(
            crate::callbacks::callback_arg(comp, ok_handler),
            crate::callbacks::callback_arg(comp, error_handler),
        )
    }
}
