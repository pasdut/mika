use super::*;
use crate::helper::ParseExt;

impl syn::parse::Parse for SimplePath {
    fn parse(input: syn::parse::ParseStream) -> syn::parse::Result<Self> {
        let mut stream = proc_macro2::TokenStream::new();
        if input.peek(syn::Ident::peek_any) {
            stream.extend(Some(input.parse::<proc_macro2::TokenTree>()?));
            while input.peek(syn::token::Colon2) && input.peek3(syn::Ident) {
                stream.extend(Some(input.parse::<proc_macro2::TokenTree>()?));
                stream.extend(Some(input.parse::<proc_macro2::TokenTree>()?));
                stream.extend(Some(input.parse::<proc_macro2::TokenTree>()?));
            }
        }
        Ok(SimplePath(stream))
    }
}

impl syn::parse::Parse for ElementConstructorCall {
    fn parse(input: syn::parse::ParseStream) -> syn::parse::Result<Self> {
        let mut constructor_call = proc_macro2::TokenStream::new();
        let mut last_ident = None;
        while input.peek(syn::Ident::peek_any) && input.peek2(syn::token::Colon2) {
            let ident = input.parse::<proc_macro2::TokenTree>()?;
            if let proc_macro2::TokenTree::Ident(ref ident) = ident {
                last_ident = Some(ident.clone());
            }
            constructor_call.extend(Some(ident));
            constructor_call.extend(Some(input.parse::<proc_macro2::TokenTree>()?));
            constructor_call.extend(Some(input.parse::<proc_macro2::TokenTree>()?));
        }
        if input.peek(crate::keywords::new) {
            constructor_call.extend(Some(input.parse::<proc_macro2::TokenTree>()?));
        } else {
            return Err(input.error("expect `new`"));
        }
        if input.peek(syn::token::Paren) {
            constructor_call.extend(Some(input.parse::<proc_macro2::TokenTree>()?));
        } else {
            return Err(input.error("expect `()`"));
        }

        // println!(
        //     "{}: {}",
        //     last_ident.as_ref().unwrap().to_string(),
        //     constructor_call.to_string()
        // );

        Ok(Self {
            constructor_call,
            element_type: last_ident.unwrap(),
        })
    }
}

impl syn::parse::Parse for ImplKeyedItem {
    fn parse(input: syn::parse::ParseStream) -> syn::parse::Result<Self> {
        let impl_token: syn::token::Impl = input.parse()?;
        let item_type: SimplePath = input.parse()?;

        let fn_render: FnRender = input.parse_brace()?;

        Ok(Self {
            impl_token,
            item_type,
            fn_render,
        })
    }
}

impl syn::parse::Parse for FnRender {
    fn parse(input: syn::parse::ParseStream) -> syn::parse::Result<Self> {
        let fn_token: syn::token::Fn = input.parse()?;
        let fn_name: crate::keywords::render = input.parse()?;

        let args: FnRenderArgs = input.parse_paren()?;

        let _: syn::token::RArrow = input.parse()?;
        let return_type: syn::Path = input.parse()?;

        let body: FnRenderBody = input.parse_brace()?;

        Ok(Self {
            fn_token,
            fn_name,
            args,
            return_type,
            body,
        })
    }
}

impl syn::parse::Parse for FnRenderArgs {
    fn parse(input: syn::parse::ParseStream) -> syn::parse::Result<Self> {
        let and_for_self: syn::token::And = input.parse()?;
        let self_ident: syn::token::SelfValue = input.parse()?;
        let _: syn::token::Comma = input.parse()?;

        let comp_ident: syn::Ident = input.parse()?;
        let _: syn::token::Colon = input.parse()?;

        let and_for_comp_handle: syn::token::And = input.parse()?;
        let mika_comp_handle_type: SimplePath = input.parse()?;

        let _: syn::token::Lt = input.parse()?;
        let user_component_type: SimplePath = input.parse()?;
        let _: syn::token::Gt = input.parse()?;

        Ok(Self {
            and_for_self,
            self_ident,
            comp_ident,
            and_for_comp_handle,
            mika_comp_handle_type,
            user_component_type,
        })
    }
}

impl syn::parse::Parse for FnRenderBody {
    fn parse(input: syn::parse::ParseStream) -> syn::parse::Result<Self> {
        let mut uses: Vec<proc_macro2::TokenStream> = Vec::new();
        let mut lets: Vec<proc_macro2::TokenStream> = Vec::new();

        while input.peek(syn::token::Use) {
            uses.push(input.parse_until_semi()?);
        }

        while input.peek(syn::token::Let) {
            lets.push(input.parse_until_semi()?);
        }

        let element: Element = input.parse()?;

        Ok(Self {
            uses,
            lets,
            element,
        })
    }
}

// impl syn::parse::Parse for Element {
//     fn parse(input: syn::parse::ParseStream) -> syn::parse::Result<Self> {
//         let element_constructor_call: ElementConstructorCall = input.parse()?;
//         let mut attribute_method_calls: Vec<AttributeMethodCall> = Vec::new();
//         let mut sub_item_method_calls: Vec<SubItemMethodCall> = Vec::new();
//         let mut need_update = false;
//         while !input.is_empty() {
//             let mc = input.parse()?;
//             if !need_update {
//                 need_update = match mc {
//                     MethodCall::SubItem(_) => false,
//                     MethodCall::Attribute(ref a) => match a {
//                         AttributeMethodCall::RenderOnly { .. } => false,
//                         AttributeMethodCall::RenderUpdate(_) => true,
//                         AttributeMethodCall::TemplateOnly(_) => false,
//                     },
//                 };
//             }
//             match mc {
//                 MethodCall::Attribute(a) => attribute_method_calls.push(a),
//                 MethodCall::SubItem(s) => sub_item_method_calls.push(s),
//             }
//             // method_calls.push(mc);
//         }
//         Ok(Self {
//             need_update,
//             generated_ident: GeneratedIdent::init(),
//             element_constructor_call,
//             // method_calls,
//             attribute_method_calls,
//             sub_item_method_calls,
//         })
//     }
// }

impl syn::parse::Parse for Element {
    fn parse(input: syn::parse::ParseStream) -> syn::parse::Result<Self> {
        let element_constructor_call: ElementConstructorCall = input.parse()?;
        let mut method_calls: Vec<MethodCall> = Vec::new();
        let mut need_update = false;
        while !input.is_empty() {
            let mc = input.parse()?;
            if !need_update {
                need_update = match mc {
                    MethodCall::SubItem(_) => false,
                    MethodCall::Attribute(ref a) => match a {
                        AttributeMethodCall::RenderOnly { .. } => false,
                        AttributeMethodCall::RenderUpdate(_) => true,
                        AttributeMethodCall::TemplateOnly(_) => false,
                    },
                };
            }
            method_calls.push(mc);
        }
        Ok(Self {
            need_update,
            generated_ident: GeneratedIdent::init(),
            element_constructor_call,
            method_calls,
        })
    }
}

impl syn::parse::Parse for MethodCall {
    fn parse(input: syn::parse::ParseStream) -> syn::parse::Result<Self> {
        let _: syn::token::Dot = input.parse()?;
        let mc = if let Some(cmc) = SubItemMethodCall::parse(input)? {
            MethodCall::SubItem(cmc)
        } else {
            MethodCall::Attribute(input.parse()?)
        };
        let _: Option<syn::token::Comma> = input.parse()?;
        Ok(mc)
    }
}

impl SubItemMethodCall {
    fn parse(input: syn::parse::ParseStream) -> syn::parse::Result<Option<Self>> {
        let cmc = if input.peek(crate::keywords::text) {
            SubItemMethodCall::Text(input.parse()?)
        } else if input.peek(crate::keywords::const_text) {
            SubItemMethodCall::ConstText(input.parse()?)
        } else if input.peek(crate::keywords::child) {
            SubItemMethodCall::Child(input.parse()?)
        } else if input.peek(crate::keywords::child_signal) {
            // ChildMethodCall::ChildSignal(input.parse()?)
            return Err(input.error(
                "`child_signal` is not supported in mika::KeyedItem (at least, currently)",
            ));
        } else if input.peek(crate::keywords::child_list)
            || input.peek(crate::keywords::list_signal)
            || input.peek(crate::keywords::list_signal_vec)
            || input.peek(crate::keywords::keyed_list_signal_vec)
        {
            // ChildMethodCall::List(input.parse()?)
            return Err(input.error(
                "`list` methods are not supported in mika::KeyedItem (at least, currently)",
            ));
        } else {
            return Ok(None);
        };
        Ok(Some(cmc))
    }
}

impl syn::parse::Parse for TextMethodCall {
    fn parse(input: syn::parse::ParseStream) -> syn::parse::Result<Self> {
        let name_args: NameArgs = input.parse()?;
        Ok(Self {
            generated_ident: GeneratedIdent::init(),
            name_args,
        })
    }
}

impl syn::parse::Parse for ChildMethodCall {
    fn parse(input: syn::parse::ParseStream) -> syn::parse::Result<Self> {
        let name: syn::Ident = input.parse()?;
        let element: Element = input.parse_paren()?;
        Ok(Self { name, element })
    }
}

impl syn::parse::Parse for NameArgs {
    fn parse(input: syn::parse::ParseStream) -> syn::parse::Result<Self> {
        let name: syn::Ident = input.parse()?;
        let args: proc_macro2::TokenStream = input.parse_paren()?;
        Ok(NameArgs { name, args })
    }
}

impl syn::parse::Parse for AttributeMethodCall {
    fn parse(input: syn::parse::ParseStream) -> syn::parse::Result<Self> {
        let name_args: NameArgs = input.parse()?;
        let name = name_args.name.to_string();
        let amc = if name.starts_with("on_") || name.ends_with("_signal") {
            AttributeMethodCall::RenderOnly {
                must_keep_alive: true,
                name_args,
            }
        } else if TEMPLATE_ONLY_METHODS.contains(name.as_str()) {
            AttributeMethodCall::TemplateOnly(name_args)
        } else if RENDER_ONLY_METHODS.contains(name.as_str()) {
            AttributeMethodCall::RenderOnly {
                must_keep_alive: false,
                name_args,
            }
        } else {
            AttributeMethodCall::RenderUpdate(name_args)
        };
        Ok(amc)
    }
}
