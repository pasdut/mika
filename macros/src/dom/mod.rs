// In the first draft, this module just implements a very simple DOM tree to work with
// mika::list::KeyedItem.
//
// Eventually, the DOM tree will extend (or re design) to validate everything
// requires by HTML specification such as child elements, attribute's name (is
// it possible to validate the attribute value by procedural macro?)...

mod parse;

// TODO: Avoid double look up at these static. Currently, it lookup twice:
// the first is in the parse phase, the second is in generate phase
lazy_static::lazy_static! {
    static ref HTML_EVENTS: std::collections::HashSet<&'static str> = {
        ["on_blur", "on_change", "on_click", "on_double_click", "on_input", "on_key_press"].iter().map(|s| *s).collect()
    };

    static ref NAME_MAP: std::collections::HashMap<&'static str, &'static str> = {
        [
            ("onblur", "on_blur"),
            ("onchange", "on_change"),
            ("onclick", "on_click"),
            ("ondoubleclick", "on_double_click"),
            ("ondblclick", "on_double_click"),
            ("oninput", "on_input"),
            ("onkeypress", "on_key_press"),
        ].iter().map(|m| *m).collect()
    };
}

fn get_normalize_name(ident: &syn::Ident) -> String {
    let name = ident.to_string();
    match NAME_MAP.get(name.as_str()) {
        Some(name) => name.to_string(),
        None => name,
    }
}

fn is_html_event(ident: &syn::Ident) -> bool {
    let name = get_normalize_name(ident);
    //HTML_EVENTS.get(name.as_str()).is_some()
    HTML_EVENTS.contains(name.as_str())
}

fn make_method_name_from_ident(ident: &syn::Ident, keyword: bool, by_signal: bool) -> syn::Ident {
    let name = get_normalize_name(ident);
    match (keyword, by_signal) {
        (true, true) => quote::format_ident!("r#{}_signal", span = ident.span(), name),
        (true, false) => quote::format_ident!("r#{}", span = ident.span(), name),
        (false, true) => quote::format_ident!("{}_signal", span = ident.span(), name),
        (false, false) => quote::format_ident!("{}", span = ident.span(), name),
    }
}

pub struct Context {
    type_info: TypeInfos,
    ident_generator: crate::helper::IdentGenerator,
}

impl Context {
    pub fn init() -> Self {
        Self {
            type_info: TypeInfos::init(),
            ident_generator: crate::helper::IdentGenerator::init(),
        }
    }
}

struct TypeInfo {
    path: proc_macro2::TokenStream,
    values: std::collections::HashMap<&'static str, &'static str>,
}

struct TypeInfos {
    infos: std::collections::HashMap<&'static str, TypeInfo>,
}

impl TypeInfos {
    fn init() -> Self {
        let mut infos = std::collections::HashMap::new();

        infos.insert(
            "input type",
            TypeInfo {
                path: quote::quote! {mika::dom::InputType},
                values: [("checkbox", "CheckBox")].into_iter().map(|v| *v).collect(),
            },
        );
        Self { infos }
    }

    fn get_attribute_enum_values(
        &self,
        html_element_tag: &syn::Ident,
        attribute_name: &syn::Ident,
        attribute_value: &syn::LitStr,
    ) -> proc_macro2::TokenStream {
        let tag_attr = format!("{} {}", html_element_tag, attribute_name);
        match self.infos.get(tag_attr.as_str()) {
            Some(info) => {
                let value = info.values.get(attribute_value.value().as_str()).unwrap();
                let value = quote::format_ident!("{}", span = attribute_value.span(), value);
                let path = &info.path;
                quote::quote! { &#path::#value }
            }
            None => quote::quote! { #attribute_value },
        }
    }
}
#[derive(Debug)]
pub struct KeyedItemTemplate {
    pub component_name: Option<syn::Path>,
    pub init_block: Option<proc_macro2::TokenStream>,
    pub html_element: HtmlElement,
}

#[derive(Debug)]
pub struct ItemList(pub Vec<Item>);

#[derive(Debug)]
pub enum Item {
    HtmlElement(HtmlElement),

    // A literal str can be passed as `&str` argument without `.to_string()`
    TextLiteralStr(syn::Expr),

    // Other literals that must be converted `.to_string()` before passing to a `&str` argument
    TextOtherLiteral(syn::Expr),

    // An expression prefixed by a `parse::ConstantSymbol`, this text will not be updated.
    // This is used for `mika::KeyedItem`, the constant text will be set by `mika::KeyedItem::render`
    // by ignored by `mika::KeyedItem::update`
    TextSimpleExpressionAsConstant(syn::Expr),

    // In `mika::KeyedItem`, this text will be set by both `mika::KeyedItem::render`
    // and `mika::KeyedItem::update`.
    // Otherwise, it is set by the normal code (not a text from signal)
    TextSimpleExpression(syn::Expr),

    // Text from signal, will be constructed by `element.text_signal`
    TextSignal(syn::Expr),
}

#[derive(Debug)]
pub struct HtmlElement {
    pub tag: syn::Ident,
    pub attributes: Attributes,
    pub child_items: ItemList,
}

#[derive(Debug)]
pub struct Attributes {
    pub literal_classes: Vec<syn::LitStr>,
    pub conditional_classes: Vec<ConditionalClass>,
    pub attributes: Vec<Attribute>,
    pub events: Vec<ElementEvent>,
}

#[derive(Debug)]
pub struct ConditionalClass {
    pub class_name: syn::LitStr,
    pub by_signal: bool,
    pub bool_expression: syn::Expr,
}

#[derive(Debug)]
pub struct Attribute {
    pub name: syn::Ident,
    pub rust_keyword: bool,
    pub by_signal: bool,
    pub value: AttributeValue,
}

#[derive(Debug)]
pub enum AttributeValue {
    Lit(syn::Lit),
    Expr(syn::Expr),
}

#[derive(Debug)]
pub struct ElementEvent {
    pub name: syn::Ident,
    pub message: ElementEventMessage,
}

#[derive(Debug)]
pub struct ElementEventMessage {
    pub path: syn::Path,
    pub args: Option<ElementEventMessageArgs>,
}

#[derive(Debug)]
pub struct ElementEventMessageArgs {
    pub receive_js_argument: bool,
    pub args: proc_macro2::TokenStream,
}

enum GenerationMode {
    Normal,
    KeyedItemTemplate,
}

impl Default for ItemList {
    fn default() -> Self {
        ItemList(Vec::new())
    }
}

impl Default for Attributes {
    fn default() -> Self {
        Attributes {
            literal_classes: Vec::new(),
            conditional_classes: Vec::new(),
            attributes: Vec::new(),
            events: Vec::new(),
        }
    }
}

impl KeyedItemTemplate {
    pub fn impl_keyed_item(
        &self,
        context: &Context,
        item_name: &syn::Ident,
    ) -> proc_macro2::TokenStream {
        let component_name = self
            .component_name
            .as_ref()
            .map(|v| quote::quote! {#v})
            .unwrap_or_else(|| quote::quote! { C });
        let init_block = self
            .init_block
            .as_ref()
            .map(|v| quote::quote! { #v})
            .unwrap_or_else(proc_macro2::TokenStream::new);
        let render_output = self.html_element.get_mika_html_item_name();
        let to_update_type = self.html_element.get_keyed_item_to_update_type();
        let fn_template_body = self
            .html_element
            .generate(context, &GenerationMode::KeyedItemTemplate);
        let fn_render_body = self
            .html_element
            .generate_keyed_item_fn_render_body(context);
        let fn_update_body = self.html_element.generate_keyed_item_fn_update_body();
        quote::quote! {
            impl mika::KeyedItem<#component_name> for #item_name {
                type RenderOutput = mika::dom::#render_output;
                type ToUpdate = #to_update_type;

                fn template() -> Self::RenderOutput {
                    use mika::prelude::*;
                    #fn_template_body.into()
                }

                fn render(&self, comp: &mika::CompHandle<#component_name>, item: Self::RenderOutput)
                    -> (Self::RenderOutput, Self::ToUpdate)
                {
                    use mika::dom::traits::Node;
                    #init_block
                    #fn_render_body
                }

                fn update(&self,comp: &mika::CompHandle<#component_name>, to_update: &Self::ToUpdate) {
                    #fn_update_body
                }
            }
        }
    }
}

impl Item {
    fn number_of_item_need_to_update(&self) -> usize {
        match self {
            Item::HtmlElement(element) => element.number_of_item_need_to_update(),
            Item::TextOtherLiteral(_)
            | Item::TextLiteralStr(_)
            | Item::TextSimpleExpressionAsConstant(_)
            | Item::TextSignal(_) => 0,
            Item::TextSimpleExpression(_) => 1,
        }
    }

    fn generate(&self, context: &Context, mode: &GenerationMode) -> proc_macro2::TokenStream {
        match self {
            Item::HtmlElement(e) => {
                let child = e.generate(context, mode);
                quote::quote! {
                    .child(#child)
                }
            }
            Item::TextOtherLiteral(e) => {
                quote::quote! {
                    .text(&#e.to_string())
                }
            }
            Item::TextLiteralStr(e) => {
                quote::quote! {
                    .text(#e)
                }
            }
            Item::TextSimpleExpressionAsConstant(e) => {
                quote::quote! {
                    .text(&#e)
                }
            }
            Item::TextSimpleExpression(e) => match mode {
                GenerationMode::Normal => {
                    quote::quote! {
                        .text(&#e)
                    }
                }
                GenerationMode::KeyedItemTemplate => {
                    quote::quote! {
                        .text("?")
                    }
                }
            },
            Item::TextSignal(e) => {
                quote::quote! {
                    .text_signal(#e)
                }
            }
        }
    }

    fn generate_render_code_for_keyed_item(
        &self,
        context: &Context,
        ws_node: &syn::Ident,
        to_updates: &mut Vec<syn::Ident>,
    ) -> proc_macro2::TokenStream {
        quote::quote! {}
    }
}

impl HtmlElement {
    fn get_mika_html_item_name(&self) -> syn::Ident {
        let mut tag = self.tag.to_string();
        let first_upper_char = tag.remove(0).to_ascii_uppercase();
        tag.insert(0, first_upper_char);
        proc_macro2::Ident::new(&tag, self.tag.span())
    }

    fn number_of_item_need_to_update(&self) -> usize {
        let count = self.child_items.0.iter().fold(0, |count, item| {
            count + item.number_of_item_need_to_update()
        });
        if self.attributes.need_update() {
            count + 1
        } else {
            count
        }
    }

    // The `mika::KeyedItem::ToUpdate`
    fn get_keyed_item_to_update_type(&self) -> proc_macro2::TokenStream {
        match self.number_of_item_need_to_update() {
            0 => quote::quote! { () },
            1 => quote::quote! { mika::WebSysNode },
            count => {
                let items = (0..count).map(|_| quote::quote! { mika::WebSysNode, });
                quote::quote! {
                    (
                        #(#items)*
                    )
                }
            }
        }
    }

    fn generate(&self, context: &Context, mode: &GenerationMode) -> proc_macro2::TokenStream {
        let mika_item_name = self.get_mika_html_item_name();
        let attributes = self.attributes.generate(context, &self.tag, mode);
        let child_items = self.child_items.generate(context, mode);
        quote::quote! {
            mika::dom::#mika_item_name::new()
                #attributes
                #child_items
                //.into()
        }
    }

    fn generate_keyed_item_fn_render_body(&self, context: &Context) -> proc_macro2::TokenStream {
        let mut to_updates = Vec::new();
        let ws_node = context.ident_generator.ident_from_str("item_ws_node");
        let update_code =
            self.generate_render_code_for_keyed_item(context, &ws_node, &mut to_updates);
        assert_eq!(to_updates.len(), self.number_of_item_need_to_update(), "Internal error: the macro that generate `impl mika::KeyedItem` has a bug, please open an issue!");

        let to_updates = match to_updates.len() {
            0 => quote::quote! { () },
            1 => {
                let item = &to_updates[0];
                quote::quote! { #item }
            }
            _ => quote::quote! { #(#to_updates)* },
        };

        quote::quote! {
            let #ws_node = item.websys_node();
            #update_code
            (item, #to_updates)
        }
    }

    fn generate_render_code_for_keyed_item(
        &self,
        context: &Context,
        ws_node: &syn::Ident,
        to_updates: &mut Vec<syn::Ident>,
    ) -> proc_macro2::TokenStream {
        if self.attributes.need_update() {
            to_updates.push(ws_node.clone());
        }

        let attributes = self
            .attributes
            .generate_render_code_for_keyed_item(context, &self.tag, ws_node);

        let mut last_ws_node = None;

        let render_childs = self.child_items.0.iter().map(|item| {
            let item_ws_node = context.ident_generator.ident_from_str("a_child_ws_node");
            let render_child =
                item.generate_render_code_for_keyed_item(context, &item_ws_node, to_updates);
            let get_item_ws_node = last_ws_node
                .as_ref()
                .map(|last| {
                    quote::quote! {
                        #last.next_sibling().unwrap_throw()
                    }
                })
                .unwrap_or_else(|| {
                    quote::quote! {
                        #ws_node.first_child().unwrap_throw()
                    }
                });
            last_ws_node = Some(item_ws_node.clone());
            quote::quote! {
                let #item_ws_node = #get_item_ws_node;
                #render_child
            }
        });

        quote::quote! {
            #attributes
            #(#render_childs)*
        }
    }

    fn generate_keyed_item_fn_update_body(&self) -> proc_macro2::TokenStream {
        quote::quote! {}
    }
}

impl Attributes {
    fn need_update(&self) -> bool {
        // :D These are certainly the worst hacks, :P (definitely need a real solution here)
        self.conditional_classes.iter().any(|cc| {
            let be = &cc.bool_expression;
            !quote::quote!(#be)
                .to_string()
                .starts_with("comp . render ()")
        });
        self.attributes.iter().any(|a| match &a.value {
            AttributeValue::Expr(e) => !quote::quote!(#e)
                .to_string()
                .starts_with("comp . render ()"),
            AttributeValue::Lit(_) => false,
        })
    }

    fn generate(
        &self,
        context: &Context,
        html_tag: &syn::Ident,
        mode: &GenerationMode,
    ) -> proc_macro2::TokenStream {
        let classes = match self.literal_classes.len() {
            0 => proc_macro2::TokenStream::new(),
            _ => {
                let classes = self
                    .literal_classes
                    .iter()
                    .map(|classes| format!("{} ", classes.value()))
                    .collect::<String>();
                let classes = classes.trim();
                quote::quote! {
                    .class(#classes)
                }
            }
        };

        let conditional_classes = self
            .conditional_classes
            .iter()
            .map(|cc| cc.generate(context, mode));
        let attributes = self
            .attributes
            .iter()
            .map(|a| a.generate(context, html_tag, mode));
        let events = match mode {
            GenerationMode::KeyedItemTemplate => vec![proc_macro2::TokenStream::new()],
            GenerationMode::Normal => self
                .events
                .iter()
                .map(|e| e.generate(context, mode))
                .collect(),
        };
        quote::quote! {
            #classes
            #(#conditional_classes)*
            #(#attributes)*
            #(#events)*
        }
    }

    fn generate_render_code_for_keyed_item(
        &self,
        context: &Context,
        html_tag: &syn::Ident,
        ws_node: &syn::Ident,
    ) -> proc_macro2::TokenStream {
        let conditional_classes = self
            .conditional_classes
            .iter()
            .map(|cc| cc.generate_render_code_for_keyed_item(ws_node));
        // let attributes = self
        //     .attributes
        //     .iter()
        //     .map(|a| a.generate_render_code_for_keyed_item(html_tag, ws_node));
        // let events = self
        //     .events
        //     .iter()
        //     .map(|e| e.generate_render_code_for_keyed_item(ws_node));

        quote::quote! {
            #(#conditional_classes)*
            // #(#attributes)*
            // #(#events)*
        }
    }
}

impl ItemList {
    fn generate(&self, context: &Context, mode: &GenerationMode) -> proc_macro2::TokenStream {
        self.0
            .iter()
            .map(|item| item.generate(context, mode))
            .collect()
    }
}

impl ConditionalClass {
    fn generate(&self, _context: &Context, mode: &GenerationMode) -> proc_macro2::TokenStream {
        match mode {
            GenerationMode::KeyedItemTemplate => proc_macro2::TokenStream::new(),
            GenerationMode::Normal => {
                let ConditionalClass {
                    class_name,
                    by_signal,
                    bool_expression,
                } = self;
                if *by_signal {
                    quote::quote! {
                        .class_signal(#class_name, #bool_expression)
                    }
                } else {
                    quote::quote! {
                        .class_if(#class_name, #bool_expression)
                    }
                }
            }
        }
    }

    fn generate_render_code_for_keyed_item(
        &self,
        ws_node: &syn::Ident,
    ) -> proc_macro2::TokenStream {
        let class_name = &self.class_name;
        let bool_expression = &self.bool_expression;
        match self.by_signal {
            true => quote::quote! {
                item.class_signal_on(&#ws_node, #class_name, #bool_expression);
            },
            false => quote::quote! {
                if bool_expression {
                    #ws_node.class_list().add_1(#class_name).unwrap_throw();
                }
                // No remove here because mika::KeyedItem::render only call
                // one time - when the node is first created
            },
        }
    }
}

impl Attribute {
    fn generate(
        &self,
        context: &Context,
        tag: &syn::Ident,
        mode: &GenerationMode,
    ) -> proc_macro2::TokenStream {
        let method_name =
            make_method_name_from_ident(&self.name, self.rust_keyword, self.by_signal);
        match self.value {
            AttributeValue::Lit(ref value) => {
                let value = match value {
                    syn::Lit::Str(value) => context
                        .type_info
                        .get_attribute_enum_values(tag, &self.name, value),
                    value => quote::quote! { #value },
                };
                quote::quote! {
                    .#method_name(#value)
                }
            }
            AttributeValue::Expr(ref e) => match mode {
                GenerationMode::KeyedItemTemplate => proc_macro2::TokenStream::new(),
                GenerationMode::Normal => {
                    quote::quote! {
                        .#method_name(#e)
                    }
                }
            },
        }
    }

    fn generate_render_code_for_keyed_item(
        &self,
        ws_node: &syn::Ident,
    ) -> proc_macro2::TokenStream {
        // need
        //     attribute_name_on()
        //     attribute_name_signal_on()
        quote::quote! {}
    }
}

impl ElementEvent {
    fn generate(&self, context: &Context, mode: &GenerationMode) -> proc_macro2::TokenStream {
        let method_name = get_normalize_name(&self.name);
        let handler = self.message.generate(context, mode);
        quote::quote! {
            .#method_name(#handler)
        }
    }
}

impl ElementEventMessage {
    fn generate(&self, context: &Context, mode: &GenerationMode) -> proc_macro2::TokenStream {
        let path = &self.path;
        match self.args {
            Some(ref args) => {
                let other_args = &args.args;
                if args.receive_js_argument {
                    quote::quote! {
                        mika::callbacks::event_handlers::fn_with_arg(comp, move |__arg| #path(__arg #other_args))
                    }
                } else {
                    quote::quote! {
                        mika::callbacks::event_handlers::fn_no_arg(comp, move || #path(#other_args))
                    }
                }
            }
            None => quote::quote! {
                mika::callbacks::event_handlers::fn_no_arg(comp, move || #path)
            },
        }
    }
}
