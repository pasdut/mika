use mika::prelude::*;
use signals::signal::Mutable;

pub trait ValueReceiver {
    fn receive_value_from_child_component(&mut self, value: i32);
}
pub struct SendBackCounter<C>
where
    C: mika::Component,
    C::State: ValueReceiver,
{
    parent_handle: mika::CompHandle<C>,
    value: i32,
}

impl<C> SendBackCounter<C>
where
    C: 'static + mika::Component,
    C::State: ValueReceiver,
{
    pub fn new(value: i32, parent_handle: mika::CompHandle<C>) -> Self {
        Self {
            parent_handle,
            value,
        }
    }

    pub fn set_value_from_parent(&mut self, value: i32) {
        self.value = value;
    }

    fn up(&mut self) {
        self.value += 10;
    }

    fn down(&mut self) {
        self.value -= 10;
    }

    fn send_value_to_parent(&mut self) {
        self.parent_handle
            .update(&|comp| comp.receive_value_from_child_component(self.value));
    }
}

impl<C> Drop for SendBackCounter<C>
where
    C: mika::Component,
    C::State: ValueReceiver,
{
    fn drop(&mut self) {
        log::info!("Dropping SendBackCounter");
    }
}

#[derive(Default)]
pub struct SendBackCounterComp<C> {
    value: Mutable<i32>,
    phantom: std::marker::PhantomData<C>,
}

impl<C> mika::Component for SendBackCounterComp<C>
where
    C: 'static + mika::Component,
    C::State: ValueReceiver,
{
    type State = SendBackCounter<C>;
    type ListChange = ();
    type Output = mika::dom::Div;

    fn diff(&self, data: &SendBackCounter<C>, _: ()) {
        self.value.set_neq(data.value);
    }

    fn render(&self, comp: &mika::CompHandle<Self>) -> Self::Output {
        mika::dom::Div::new()
            .child(
                crate::stateless_view_renders::CounterView {
                    title: "A SendBackCounter",
                    signal: self.value.signal(),
                    up: mika::handler!(comp, SendBackCounter::up),
                    down: mika::handler!(comp, SendBackCounter::down),
                }
                .render(),
            )
            .child(mika::dom::P::new().text("The SendBackCounter is able to send its value back to the main component if you click this button"))
            .child(
                mika::dom::Button::new()
                    .text("Send value to the main component")
                    .on_click(mika::handler!(comp, SendBackCounter::send_value_to_parent)),
            )
            .into()
    }
}
