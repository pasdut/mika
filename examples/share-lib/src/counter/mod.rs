use mika::prelude::*;
use signals::signal::Mutable;

pub struct CounterState {
    pub value: i32,
}

impl Default for CounterState {
    fn default() -> Self {
        Self { value: 2019 }
    }
}

impl CounterState {
    fn down(&mut self) {
        self.value -= 1;
    }

    fn up(&mut self) {
        self.value += 1;
    }
}

#[derive(Default)]
pub struct CounterComp {
    value: Mutable<i32>,
}

impl mika::Component for CounterComp {
    type State = CounterState;
    type ListChange = ();
    type Output = mika::dom::Div;

    fn diff(&self, state: &Self::State, _: ()) {
        self.value.set_neq(state.value);
    }

    fn render(&self, comp: &mika::CompHandle<Self>) -> Self::Output {
        mika::dom::Div::new()
            .child(mika::dom::H2::new().text("Counter"))
            .child(mika::dom::P::new().text("The simplest example"))
            .child(mika::dom::Hr::new())
            .child(
                mika::dom::Div::new()
                    .child(
                        mika::dom::Button::new()
                            .text("Down")
                            .on_click(mika::handler! {comp, CounterState::down}),
                    )
                    .text(" ")
                    .text_signal(self.value.signal())
                    .text(" ")
                    .child(
                        mika::dom::Button::new()
                            .text("Up")
                            .on_click(mika::handler! {comp, CounterState::up}),
                    ),
            )
            .into()
    }
}
