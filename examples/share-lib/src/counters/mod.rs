use mika::prelude::*;
use signals::signal::{Mutable, SignalExt};

mod state;

use crate::stateless_view_renders::*;

#[derive(Default)]
pub struct Counters {
    mode: Mutable<state::CounterMode>,
    year: Mutable<i32>,
    month: Mutable<i32>,
}

impl mika::Component for Counters {
    type State = state::Counters;
    type ListChange = ();
    type Output = mika::dom::Div;

    fn diff(&self, data: &state::Counters, _: ()) {
        self.mode.set_neq(data.mode);
        self.year.set_neq(data.year);
        self.month.set_neq(data.month);
    }

    fn render(&self, comp: &mika::CompHandle<Self>) -> Self::Output {
        mika::dom::Div::new()
            .child(mika::dom::H2::new().text("Counters"))
            .child(
                mika::dom::P::new()
                    .text("Split your render into stateless renders and reused them"),
            )
            .child(mika::dom::Hr::new())
            .child(self.mode_list(comp))
            .child(
                CounterView {
                    title: "Year",
                    signal: self.year.signal(),
                    up: mika::handler! { comp, state::Counters::year_up },
                    down: mika::handler! { comp, state::Counters::year_down },
                }
                .render(),
            )
            .child_signal(self.mode.signal().map({
                let month = self.month.clone();
                let comp = comp.clone();
                move |mode| match mode {
                    state::CounterMode::SingleStep => CounterView {
                        title: "Month",
                        signal: month.signal(),
                        up: mika::handler! { &comp, state::Counters::month_up(1) },
                        down: mika::handler! { &comp, state::Counters::month_down(1) },
                    }
                    .render(),
                    state::CounterMode::MultiSteps => MultiStepCounterView {
                        title: "Month",
                        signal: month.signal(),
                        up_1: mika::handler! { &comp, state::Counters::month_up(1) },
                        down_1: mika::handler! { &comp, state::Counters::month_down(1) },
                        up_5: mika::handler! { &comp, state::Counters::month_up(5) },
                        down_5: mika::handler! { &comp, state::Counters::month_down(5) },
                    }
                    .render(),
                }
            }))
            .into()
    }
}

impl Counters {
    fn mode_list(&self, comp: &mika::CompHandle<Counters>) -> mika::dom::FieldSet {
        crate::stateless_view_renders::ModeList::new("mode", "Select a mode")
            .add(
                state::CounterMode::SingleStep,
                self.mode.signal(),
                comp,
                state::Counters::set_mode,
            )
            .add(
                state::CounterMode::MultiSteps,
                self.mode.signal(),
                comp,
                state::Counters::set_mode,
            )
            .done()
    }
}
