use mika::prelude::*;

pub struct CounterView<'a, S, F1, F2> {
    pub title: &'a str,
    pub signal: S,
    pub up: F1,
    pub down: F2,
}

impl<'a, S, T, F1, F2> CounterView<'a, S, F1, F2>
where
    S: 'static + signals::signal::Signal<Item = T>,
    T: 'static + ToString,
    F1: mika::events::Click,
    F2: mika::events::Click,
{
    pub fn render(self) -> mika::dom::Div {
        mika::dom::Div::new()
            .child(mika::dom::H2::new().text(self.title))
            .child(mika::dom::Button::new().text("Down").on_click(self.down))
            .text(" ")
            .text_signal(self.signal)
            .text(" ")
            .child(mika::dom::Button::new().text("Up").on_click(self.up))
            .into()
    }
}
