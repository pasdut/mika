use mika::prelude::*;
use signals::signal::Mutable;

pub trait CommunicateWithCounter1: Sized {
    fn set_counter_1(&self, counter_1: &std::rc::Rc<Counter1<Self>>);
    fn set_value_from_counter_1(&self, value: i32);
}

pub struct Counter1<M: CommunicateWithCounter1> {
    main_comp: std::rc::Rc<M>,
    value: Mutable<i32>,
}

impl<M: CommunicateWithCounter1> Counter1<M> {
    pub fn new(main_comp: &std::rc::Rc<M>) -> std::rc::Rc<Self> {
        let c1 = std::rc::Rc::new(Self {
            main_comp: std::rc::Rc::clone(main_comp),
            value: Mutable::new(2019),
        });
        main_comp.set_counter_1(&c1);
        c1
    }

    fn send_value_to_main_component(&self) {
        self.main_comp.set_value_from_counter_1(self.value.get());
    }

    fn down(&self) {
        self.value.replace_with(|x| *x - 1);
    }

    fn up(&self) {
        self.value.replace_with(|x| *x + 1);
    }
}

impl<M: 'static + CommunicateWithCounter1> mika::RawComponent for Counter1<M> {
    type RenderOutput = mika::dom::Div;
    fn render(comp: &std::rc::Rc<Self>) -> Self::RenderOutput {
        crate::counter_view::CounterView {
            title: "Counter 1",
            signal: comp.value.signal(),
            up: mika::raw_handler! {comp.up()},
            down: mika::raw_handler! {comp.down()},
        }
        .render()
        .text(" ")
        .child(
            mika::dom::Button::new()
                .text("Send value to main component")
                .on_click(mika::raw_handler!(comp.send_value_to_main_component())),
        )
        .into()
    }
}

pub struct Counter2 {
    value: Mutable<i32>,
}

impl Counter2 {
    pub fn new() -> Self {
        Self {
            value: Mutable::new(6),
        }
    }
    fn down(&self) {
        self.value.replace_with(|x| *x - 2);
    }
    fn up(&self) {
        self.value.replace_with(|x| *x + 2);
    }
}

impl mika::RawComponent for Counter2 {
    type RenderOutput = mika::dom::Div;
    fn render(comp: &std::rc::Rc<Self>) -> Self::RenderOutput {
        crate::counter_view::CounterView {
            title: "Counter 2",
            signal: comp.value.signal(),
            up: mika::raw_handler! {comp.up()},
            down: mika::raw_handler! {comp.down()},
        }
        .render()
    }
}

pub struct PermanentComponent {
    value: Mutable<i32>,
}

impl PermanentComponent {
    pub fn new() -> Self {
        Self {
            value: Mutable::new(0),
        }
    }
    fn up(&self) {
        self.value.replace_with(|x| *x + 3);
    }

    fn down(&self) {
        self.value.replace_with(|x| *x - 3);
    }
}

impl mika::RawComponent for PermanentComponent {
    type RenderOutput = mika::dom::Div;
    fn render(comp: &std::rc::Rc<Self>) -> Self::RenderOutput {
        crate::counter_view::CounterView {
            title: "Permanent component",
            signal: comp.value.signal(),
            up: mika::raw_handler! {comp.up()},
            down: mika::raw_handler! {comp.down()},
        }
        .render()
    }
}

impl<M: CommunicateWithCounter1> Drop for Counter1<M> {
    fn drop(&mut self) {
        log::info!("Dropping counter 1");
    }
}

impl Drop for Counter2 {
    fn drop(&mut self) {
        log::info!("Dropping counter 2");
    }
}

impl Drop for PermanentComponent {
    fn drop(&mut self) {
        log::info!("Dropping permanent counter");
    }
}
