use mika::prelude::*;
use signals::signal::Mutable;

pub struct Counter {
    value: Mutable<i32>,
}

impl Default for Counter {
    fn default() -> Self {
        Self {
            value: Mutable::new(2019),
        }
    }
}

impl Counter {
    fn up(&self) {
        self.value.replace_with(|x| *x + 1);
    }
    fn down(&self) {
        self.value.replace_with(|x| *x - 1);
    }
}

impl mika::RawComponent for Counter {
    type RenderOutput = mika::dom::Div;
    fn render(comp: &std::rc::Rc<Self>) -> Self::RenderOutput {
        mika::dom::Div::new()
            .child(mika::dom::H2::new().text("Counter"))
            .child(mika::dom::P::new().text("The simplest example for RawComponent"))
            .child(mika::dom::Hr::new())
            .child(
                mika::dom::Div::new()
                    .child(
                        mika::dom::Button::new()
                            .text("Down")
                            .on_click(mika::raw_handler! {comp.down()}),
                    )
                    .text(" ")
                    .text_signal(comp.value.signal())
                    .text(" ")
                    .child(
                        mika::dom::Button::new()
                            .text("Up")
                            .on_click(mika::raw_handler! {comp.up()}),
                    ),
            )
            .into()
    }
}
