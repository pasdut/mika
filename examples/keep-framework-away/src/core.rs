#[derive(Clone, PartialEq)]
pub struct Item {
    pub id: u32,
    pub name: String,
}

#[derive(Default)]
pub struct Data {
    items: Vec<Item>,
}

impl Data {
    pub fn append_item(&mut self) {
        let id = self.items.len() as u32 + 1;
        self.items.push(Item {
            id,
            name: format!("Item #{}", id),
        });
    }

    pub fn pop(&mut self) -> Option<Item> {
        self.items.pop()
    }

    pub fn items(&self) -> &Vec<Item> {
        &self.items
    }
}
