use mika::prelude::*;
use wasm_bindgen::prelude::*;

// Think this `core` module is an external crate and we do not want to
// implement Mika's traits on their items.
mod core;

#[derive(Default)]
struct State {
    data: core::Data,
}

impl State {
    fn append_item(&mut self) {
        self.data.append_item()
    }

    fn pop(&mut self) {
        self.data.pop();
    }
}

#[derive(Default)]
struct Comp {
    items: signals::signal_vec::MutableVec<ItemView>,
}

mika::create_app_handle!(Comp);

impl mika::Component for Comp {
    type State = State;
    type ListChange = mika::ListChange;
    type Output = mika::dom::Body;

    fn diff(&self, state: &State, list_change: mika::ListChange) {
        list_change.relay(state.data.items(), &item_key, &self.items);
    }

    fn render(&self, comp: &mika::CompHandle<Self>) -> Self::Output {
        mika::dom::Body::get()
            .clear()
            .child(
                mika::dom::Table::new().keyed_list_signal_vec(comp, self.items.signal_vec_cloned()),
            )
            .child(
                mika::dom::Div::new()
                    .child(
                        mika::dom::Button::new()
                            .on_click(mika::handler!(comp, State::append_item))
                            .text("Push"),
                    )
                    .child(
                        mika::dom::Button::new()
                            .on_click(mika::handler!(comp, State::pop))
                            .text("Pop"),
                    ),
            )
    }
}

fn item_key(item: &core::Item) -> &u32 {
    &item.id
}

#[derive(Clone)]
struct ItemView(core::Item);

impl mika::From<core::Item> for ItemView {
    fn from(item: &core::Item) -> Self {
        Self(item.clone())
    }
}

impl PartialEq<ItemView> for core::Item {
    fn eq(&self, rhs: &ItemView) -> bool {
        self.eq(&rhs.0)
    }
}

impl mika::Key for ItemView {
    type Key = u32;
    fn key(&self) -> &u32 {
        &self.0.id
    }
}

#[mika::convert_to_impl_keyed_item]
impl ItemView {
    fn render(&self, comp: &mika::CompHandle<Comp>) -> mika::dom::Tr {
        mika::dom::Tr::new()
            .child(mika::dom::Td::new().text(&self.0.id.to_string()))
            .child(mika::dom::Td::new().text(&self.0.name))
            .into()
    }
}
